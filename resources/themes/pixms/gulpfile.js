var gulp = require('gulp'),
browserSync = require('browser-sync'),
reload = browserSync.reload,
inject = require('gulp-inject'),
autoprefixer = require('gulp-autoprefixer'),
mainBowerFiles = require('main-bower-files'),
twig = require('gulp-twig'),
rename = require('gulp-rename'),
fs = require('fs'),
amdOptimize = require('gulp-amd-optimizer'),
concat = require('gulp-concat'),
filter = require('gulp-filter');

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./public/",
            index: "login.html"
        }
    });
});

var bower_css = [
    './public/assets/vendor/normalize.css',
    './public/assets/vendor/dataTables.colReorder.css'
];
var bower_js = [
    './public/assets/vendor/jquery.js', 
    './public/assets/vendor/selectize.js',
    './public/assets/vendor/underscore.js', 
    './public/assets/vendor/backbone.js',
    './public/assets/vendor/jquery.dataTables.js',
    './public/assets/vendor/dataTables.colReorder.js',
    './public/assets/vendor/dataTables.colVis.js'
];

gulp.task('compile-html', ['compile-css'], function () {
    'use strict';
    var twig_data = JSON.parse(fs.readFileSync('./viewdata.json','utf8'));
    var twig_config = {
        base: ['./views', './base/views'],
        data: twig_data,
        errorLogToConsole: true,
        extend: function(Twig) {
            Twig.token.definitions.unshift({
            type: Twig.token.type.output,
            open: '{{-',
            close: '-}}'
        });
            Twig.token.definitions.unshift({
            type: Twig.token.type.logic,
            open: '{%-',
            close: '-%}'
        });
            Twig.filter.extend('trans', function (value) {
                return value;
            });
            return Twig;
        }
        
    };
    return gulp.src(['./views/*.twig', '!./views/**/_*.twig']).pipe(twig(twig_config)).pipe(gulp.dest('./public')).pipe(reload({stream: true}));
});

gulp.task('compile-bower', function() {
   /* var amd_config = JSON.parse(fs.readFileSync('./amdconfig.json','utf8'));
    var nojs = filter(['!*.js']);
    var js = filter(['*.js']);
    return gulp.src(mainBowerFiles()).pipe(nojs).pipe(gulp.dest('./public/assets/vendor')).pipe(nojs.restore())
            .pipe(js).pipe(amdOptimize(amd_config)).pipe(concat('vendor.js')).pipe(gulp.dest('./public/assets/vendor'));*/
    return gulp.src(mainBowerFiles()).pipe(gulp.dest('./public/assets/vendor'));
});
var css_inject = function (filepath, file, i, length) {
        return '<link rel="stylesheet" href="'+filepath.replace('/public/assets/', '{{ assets_path }}')+'">';
   };
var js_inject = function (filepath, file, i, length) {
        return '<script type="text/javascript" src="'+filepath.replace('/public/assets/', '{{ assets_path }}')+'"></script>';
   };
gulp.task('inject-bower', function() {
    /*var amd_config = JSON.parse(fs.readFileSync('./amdconfig.json','utf8'));
    var js = filter(['*.js']);
    var amd_files = gulp.src(mainBowerFiles()).pipe(js).pipe(amdOptimize(amd_config));*/

  // return gulp.src('./views/**/*.twig').pipe(inject(gulp.src(['./public/assets/vendor/*.js'], {read: false}), {name: 'bower', transform: js_inject}))
  //         .pipe(inject(gulp.src(['./public/assets/vendor/jquery.dataTables.css', './public/assets/vendor/*.css'], {read: false}), {name: 'bower', transform: css_inject})).pipe(gulp.dest('./views'));
    return gulp.src('./views/**/*.twig').pipe(inject(gulp.src(bower_js, {read: false}), {name: 'bower', transform: js_inject}))
            .pipe(inject(gulp.src(bower_css, {read: false}), {name: 'bower', transform: css_inject})).pipe(gulp.dest('./views'));
});

gulp.task('inject-css', ['compile-css'], function() {
   return gulp.src('./views/**/*.twig').pipe(inject(gulp.src(['./public/assets/css/*'], {read: false}), {name: 'core', transform: css_inject})).pipe(gulp.dest('./views'));
});

gulp.task('inject-js',function() {
   return gulp.src('./views/**/*.twig').pipe(inject(gulp.src(['./public/assets/js/third-party/*.js', './public/assets/js/modules/*.js', './public/assets/js/app.js'], {read: false}), {name: 'core', transform: js_inject})).pipe(gulp.dest('./views'));
});

gulp.task('compile-css', function () {
    'use strict';
    var sass = require('gulp-ruby-sass');
    var uglifycss = require('gulp-uglifycss');
    return sass('./scss/').pipe(autoprefixer({browsers: ['last 2 versions', '> 5%', 'ie 8', 'safari 5']}))
            .pipe(uglifycss({
                "max-line-len": 80
              }))
            .pipe(gulp.dest('./public/assets/css'))
            .pipe(reload({stream: true}));
});

gulp.task('compile-img', function () {
    'use strict';
    var imagemin = require('gulp-imagemin');
    var jpegoptim = require('imagemin-jpegoptim');
    return gulp.src('./images/**/*').pipe(imagemin({
        progressive: true,
        max: 85,
        optimizationLevel: 7,
        interlaced: true
       // use: [jpegoptim()]
    })).pipe(gulp.dest('./public/assets/images')).pipe(reload({stream: true}));
});

gulp.task('default', ['compile-css', 'compile-html']);

gulp.task('watch-php', function() {

    gulp.watch('images/**/*', ['compile-img']);
    gulp.watch('scss/**/*.scss', ['compile-css']);
    //gulp.watch(['views/**/*.twig'], ['compile-html']);
});

gulp.task('watch', ['browser-sync'], function() {

    gulp.watch('images/**/*', ['compile-img']);
    gulp.watch('scss/**/*.scss', ['compile-css']);
    gulp.watch(['views/**/*.twig'], ['compile-html']);
});
