requirejs.config({
  baseUrl: '',
  paths: {
    'jquery': '../vendor/jquery.js',
    'underscore': '../vendor/underscore.js',
    'backbone': '../vendor/backbone.js'
  },
  shim: {
    'jquery': {
      exports: '$'
    },
    'backbone': {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    },
    'underscore': {
      exports: '_'
    }
  }
});

require(['jquery'], function($) {
    console.log($);
});