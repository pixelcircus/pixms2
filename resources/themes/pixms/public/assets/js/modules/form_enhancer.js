
$(function () {

    $('select:not(.plain)').each(function () {
        $(this).selectize({
            sortField: 'text'
        });
    });
    var i = 0;
    $('*[data-editor] textarea').each(function() {
        $(this).attr('id', 'editor'+i);
        CKEDITOR.replace('editor'+i);
        i++;
    });

    $('input[type=file]').filestyle();
    $('input[type=radio]').radiostyle();
    $('input[type=checkbox]').checkboxstyle();
    $('input[type=file]').each(function() {
        $(this).parents('.filestyle-container').find('.filestyle').val($(this).attr('value'));
    });

    $('.crop-image').cropify();

    $('*[data-remove-file]').parent().find('input[type=file]').on('change', function() {
        //console.log($(this).attr('value'));
        //console.log($(this));
        var $parent = $(this).parents('.filestyle-container');
        $parent.siblings('img, .icon-edit').remove();
        if ($(this).attr('value') === '') {
            $parent.siblings('.icon-cancel').hide();
        } else {
            $parent.siblings('.icon-cancel').show();
        }
    });//.trigger('change');
    
    $('*[data-remove-file]').on('click', function(e) {
        e.preventDefault();
        $('input[type=file][name="'+$(this).data('removeFile')+'"]').parents('.filestyle-container').find('> input').val('');
        $('input[name="'+$(this).data('removeFile')+'"]').val('').trigger('change');
    });
    
});

$.fn.radiostyle = function() {
    var radiostyle = function() {
        var input = $(this);
        if (input.parents('.radiostyle-container').length) {
            return this;
        }
        input.wrap('<span class="radiostyle-container"></span>');
        input.after('<span class="radiostyle"></span>');
        input.next().on('click', function() {
            input.trigger('click');
        });
        input.on('change', function() {
           var parent = $(this).parent();
           if ($(this).is(':checked')) {
               $('input[name='+$(this).attr('name')+']').not(this).trigger('change');
               parent.addClass('radiostyle-checked');
           } else {
               parent.removeClass('radiostyle-checked');
           }
        });
        if (input.is(':checked')) {
            input.trigger('change');
        }
    };
     return this.filter('input[type=radio]').each(radiostyle);
};

$.fn.checkboxstyle = function() {
    var checkboxstyle = function() {
        var input = $(this);
        if (input.parents('.checkboxstyle-container').length) {
            return this;
        }
        input.wrap('<span class="checkboxstyle-container"></span>');
        input.after('<span class="checkboxstyle"></span>');
        input.next().on('click', function() {
            input.trigger('click');
        });
        input.on('change', function() {
           var parent = $(this).parent();
           if ($(this).is(':checked')) {
               parent.addClass('checkboxstyle-checked');
           } else {
               parent.removeClass('checkboxstyle-checked');
           }
        });
        if (input.is(':checked')) {
            input.trigger('change');
        }
    };
     return this.filter('input[type=checkbox]').each(checkboxstyle);
};

$.fn.cropify = function() {
    var cropify = function() {
        var $container = $(this);
            var format = $('select[name=format] option:selected', $container).text();
            var api;
            var $img = $('img', $container);
            var true_sizes = [$img.data('width'), $img.data('height')];
            console.log(true_sizes);
            var get_sizes = function() {
                var sizes = $('input[type=hidden][name="formats_size['+format+']"]', $container).val().split('x');
                return {
                  'width': parseInt(sizes[0]),
                  'height': parseInt(sizes[1])
                };
            };
            var get_select = function() {
                var val = $('input[type=hidden][name="formats['+format+']"]', $container).val();
                    if (val === '') {
                        var sizes = get_sizes();
                        return [0,0, sizes.width, sizes.height];
                    }
                var crop = $.deserialize(val);
                return [crop.x, crop.y, crop.x2, crop.y2];
            };

            $('select[name=format]', $container).on('change.cropify', function() {
                format = $(this).find('option:selected').text();
                var sizes = get_sizes();

                if (typeof api !== 'undefined') {
                    if (true_sizes[0] < sizes.width || true_sizes[1] < sizes.height) {
                        api.disable();
                        api.release();
                        //$('img', $container).css('opacity', 0.5);
                    } else if (api) {
                        api.enable();
                        //$('img', $container).css('opacity', 1);
                        api.setOptions({ aspectRatio: sizes.width/sizes.height, minSize: [sizes.width, sizes.height]});
                        api.setSelect(get_select());
                    }
                }
            });
            var sizes = get_sizes();
            var select = get_select();
            
            $img.Jcrop({
                trueSize: true_sizes,
                onSelect: function(data) {
                    for (var i in data) {
                        data[i] = parseInt(data[i]);
                    }
                    $('input[type=hidden][name="formats['+format+']"]', $container).val($.param(data));
                },
                onRelease: function() {
                    $('input[type=hidden][name="formats['+format+']"]', $container).val('');
                },
                aspectRatio: sizes.width/sizes.height,
                minSize: [sizes.width, sizes.height],
                setSelect: select
            },function(){
                api = this;
                $('select[name=format]', $container).trigger('change.cropify');
              });
    };
     return this.each(cropify);
};
    $.deserialize = function (str, options) {
        var pairs = str.split(/&amp;|&/i),
            h = {},
            options = options || {};
        for(var i = 0; i < pairs.length; i++) {
            var kv = pairs[i].split('=');
            kv[0] = decodeURIComponent(kv[0]);
            if(!options.except || options.except.indexOf(kv[0]) == -1) {
                if((/^\w+\[\w+\]$/).test(kv[0])) {
                    var matches = kv[0].match(/^(\w+)\[(\w+)\]$/);
                    if(typeof h[matches[1]] === 'undefined') {
                        h[matches[1]] = {};
                    }
                    h[matches[1]][matches[2]] = decodeURIComponent(kv[1]);
                } else {
                    h[kv[0]] = decodeURIComponent(kv[1]);
                }
            }
        }
        return h;
    };

    $.fn.deserialize = function (options) {
        return $.deserialize($(this).serialize(), options);
    };