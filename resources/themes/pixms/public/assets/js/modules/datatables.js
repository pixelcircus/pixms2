$(function () {
    var default_config = {
        "pagingType": "simple_numbers",
        processing: true,
        "dom": 'R<"dataTables_header"Cf>rt<"dataTables_footer"lpi>',
        "colVis": {
            "buttonText": "Colonnes",
            "overlayFade": 250,
            "showAll": "Tout afficher"
        },
        "language": {
            "search": ""
        }
    };
    var config = {};
    //TODO Make a backbone view instead
    $('table').each(function() {
        var $table = $(this);
        config = _.extend(default_config, datatable_config);
        if ($table.data('reorder')) {
            $table.DataTable(_.extend(config, {ordering: false}));            
            //TODO Use an explicit attribute instead
            var reorder = $(this).data('reorder');
            var handle = "."+$(this).data('reorder');
            $table.find('tbody').dragsort({
                dragSelector: handle,
                placeHolderTemplate: '<tr class="placeholder"></tr>',
                dragEnd: function() {
                    var $row = $(this);
                    var current = $(this).find('[data-'+reorder+']').data(reorder);
                    var prev = $(this).prev().find('[data-'+reorder+']').data(reorder);
                    var next = $(this).next().find('[data-'+reorder+']').data(reorder);
                    var direction, to;
                    if (current > prev) {
                        direction = 'backward';
                        to = next;
                    } else {
                        direction = 'forward';
                        to = prev;
                    }
                    var result = false;
                    $.ajax({
                        type: 'POST',
                        async: false,
                        url: $table.data('reorderUrl'),
                        data: {from: current, to: to, direction: direction},
                        error: function() {
                            //TODO Better error handling
                            alert('An error occurred while reordering.');
                        },
                        success: function(data) {
                            if (data.result === true) {
                                if (direction === 'forward') {
                                    $row.prevAll().each(function() {
                                        var $order = $(this).find('[data-'+reorder+']');
                                        var order = parseInt($order.data(reorder));
                                        if (order > current) {
                                            $order.data(reorder, parseInt($order.data(reorder))-1);
                                        }
                                    });
                                    
                                } else {
                                    $row.nextAll().each(function() {
                                        var $order = $(this).find('[data-'+reorder+']');
                                        var order = parseInt($order.data(reorder));
                                        if (order < current) {
                                            $order.data(reorder, parseInt($order.data(reorder))+1);
                                        }
                                    });
                                }
                                $row.find('[data-'+reorder+']').data(reorder, to);
                                result = true;
                            }
                        },
                        dataType: 'json'
                    });
                    
                    return result;
                }
            });
        } else {
            $table.DataTable(config);
        }
    });

    $('.dataTables_wrapper').each(function () {
        $('.dataTables_footer select', this).selectize({
            readOnly: true,
            onDelete: function () {
                return false;
            }
        });
        $(this).find('div.ColVis').on('click', function () {
            var checks = $(".ColVis_collection input[type=checkbox]");
            checks.checkboxstyle();

            $(".ColVis_collection .ColVis_Special").on('click', function () {
                checks.trigger('change');
            });
        });
    });
    
    $('.image-popup').magnificPopup({
        type: 'image'
    });


});