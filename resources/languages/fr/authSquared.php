<?php

return [
    'logged_out_message' => 'Vous avez été déconnecté.',
    'logged_in_message' => 'Vous êtes maintenant connecté.',
    'wrong_credentials_message' => 'Mauvaise combinaison courriel/mot de passe.',
    'unauthorized_message' => 'Vous devez être connecté pour accéder à cette page.',
    'auth:title_login' => 'Connexion',
    'auth:field_email' => 'Courriel',
    'auth:field_password' => 'Mot de passe',
    'auth:submit_login' => 'Me connecter',
    'auth:submit_send' => 'Envoyer',
    'auth:link_forgot_password' => 'J\'ai oublié mon mot de passe',
    'auth:title_forgot_password' => 'Réinitialisation du mot de passe',
    'auth:link_return_login' => 'Retour à la page de connexion',
    'auth:description_forgot_password' => 'Un courriel vous sera envoyé à l\'adresse associée avec votre compte avec un lien pour réinitialiser votre mot de passe.',
];