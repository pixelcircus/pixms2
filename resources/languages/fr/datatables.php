<?php

return [
    "emptyTable" => "Aucune entrée",
    "info" => "Affichage des entrées de _START_ à _END_ sur _TOTAL_ entrées",
    "infoEmpty" => "Affichage des entrées 0 à 0 sur 0 entrées",
    "infoFiltered" => "(filtré de _MAX_ entrées au total)",
    "infoPostFix" => "",
    "thousands" => " ",
    "lengthMenu" => "Afficher _MENU_ entrées",
    "loadingRecords" => "Chargement...",
    "processing" => "Traitement...",
    "search" => "",
    "zeroRecords" => "Aucune entrée correspondante",
    "paginate" => [
        "first" => "Premier",
        "last" => "Dernier",
        "next" => "Suivant",
        "previous" => "Précédent"
    ],
    "aria" => [
        "sortAscending" => ": activer pour trier par ordre croissant",
        "sortDescending" => ": activer pour trier par ordre décroissant"
    ]
];
