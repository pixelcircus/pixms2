<?php

return [
    'logged_out_message' => 'You have been logged out.',
    'logged_in_message' => 'You are now logged in.',
    'wrong_credentials_message' => 'Wrong credentials.',
    'unauthorized_message' => 'You need to log in to access this page.',
    'auth:title_login' => 'Login',
    'auth:field_email' => 'E-mail address',
    'auth:field_password' => 'Password',
    'auth:submit_login' => 'Log in',
    'auth:submit_send' => 'Send',
    'auth:link_forgot_password' => 'I forgot my password',
    'auth:title_forgot_password' => 'Reset password',
    'auth:link_return_login' => 'Back to login page',
    'auth:description_forgot_password' => 'An e-mail will be sent to the e-mail address linked to your account containing a link to reset your password.',
];