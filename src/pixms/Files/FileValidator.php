<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Files;

/**
 * Description of FileValidator
 *
 * @author Sonia
 */
class FileValidator
{
    protected $request;
    protected $session;
    
    public function __construct(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Session\SessionInterface $session) {
        $this->session = $session;
        $this->request = $request;
    }
    
    protected function getFile($field) {
        return $this->request->files->get($field, null);
    }
    
    public function isImage($field, $value, array $params) {
        $file = $this->getFile($field);
        
        //Ignore if no file
        if (is_null($file)) {
            return true;
        }
        
        $sizes = getimagesize($file->getPathName());
        $params['sizes'] = $sizes;
        return $sizes[0] > 0;
    }
    
    public function imageMinWidth($field, $value, array $params) {
        $file = $this->getFile($field);
        //Ignore if no file
        if (is_null($file)) {
            return true;
        }
        
            //TODO Find a way to pass the size around
            $sizes = getimagesize($file->getPathName());
            return $sizes[0] > $params[0];
        }
        
     public   function imageMinHeight($field, $value, array $params) {
        $file = $this->getFile($field);
        //Ignore if no file
        if (is_null($file)) {
            return true;
        }
            $sizes = getimagesize($file->getPathName());
            return $sizes[1] > $params[0];
        }
}
