<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Files;

/**
 * Description of ImageServer
 *
 * @author Sonia
 */
class ImageServer implements \Laasti\Route\Interfaces\EarlyResponseInterface
{
    protected $image;
    protected $files;
    protected $responder;
    protected $mapper;
    protected $formats;
    
    public function __construct(\Laasti\Response\ResponderInterface $responder, \Gregwar\Image\Image $image, \League\Flysystem\MountManager $files, \Spot\MapperInterface $mapper, $formats = [])
    {
        $this->image = $image;
        $this->responder = $responder;
        $this->mapper = $mapper;
        $this->files = $files;
        $this->formats = $formats['formats'];
    }
    
    public function crop(\Symfony\Component\HttpFoundation\Request $request)
    {
        $attr = $request->attributes->all();
        $this->image->setData($this->files->read('uploads://'.urldecode($attr['path'])));
        $path_to_image = $this->image->zoomCrop($attr['width'], $attr['height'])->guess(80);
        $image = file_get_contents($path_to_image);
        $info = getimagesize($path_to_image);
        return $this->responder->raw($image, 200, ['Content-Type' => $info['mime']]);
    }
    
    public function fit(\Symfony\Component\HttpFoundation\Request $request)
    {
        $attr = $request->attributes->all();
        $this->image->setData($this->files->read('uploads://'.urldecode($attr['path'])));
        $path_to_image = $this->image->cropResize($attr['width'], $attr['height'])->guess(80);
        $image = file_get_contents($path_to_image);
        $info = getimagesize($path_to_image);
        return $this->responder->raw($image, 200, ['Content-Type' => $info['mime']]);
    }
    public function maxwidth(\Symfony\Component\HttpFoundation\Request $request)
    {
        $attr = $request->attributes->all();
        $this->image->setData($this->files->read('uploads://'.urldecode($attr['path'])));
        $path_to_image = $this->image->resize($attr['width'])->guess(80);
        $image = file_get_contents($path_to_image);
        $info = getimagesize($path_to_image);
        return $this->responder->raw($image, 200, ['Content-Type' => $info['mime']]);
    }
    public function maxheight(\Symfony\Component\HttpFoundation\Request $request)
    {
        $attr = $request->attributes->all();
        $this->image->setData($this->files->read('uploads://'.urldecode($attr['path'])));
        $path_to_image = $this->image->resize(null, $attr['height'])->guess(80);
        $image = file_get_contents($path_to_image);
        $info = getimagesize($path_to_image);
        return $this->responder->raw($image, 200, ['Content-Type' => $info['mime']]);
    }

    public function format(\Symfony\Component\HttpFoundation\Request $request)
    {
        $attr = $request->attributes->all();
        $format = $attr['format'];
        if (!isset($this->formats[$format])) {
            //TODO Default Image
            return $this->responder->raw('Undefined Image Format: '.$format, 404);
        }
        $entity = $this->mapper->get(urldecode($attr['path']));
        $entityFormats = $entity->get('formats');
        $size = $this->formats[$format];
        $config = [];

        $data = $this->files->read('uploads://'.urldecode($attr['path']));
        $this->image->setData($data);
        $info = getimagesizefromstring($data);
        //TODO as a parameter
        $bg = "#000000";
        if (isset($entityFormats[$format]) && !empty($entityFormats[$format])) {
            parse_str($entityFormats[$format], $config);
            $this->image->crop($config['x'], $config['y'], $config['w'], $config['h'])->resize($size[0], $size[1], $bg);
        } else if ($info[0] < $size[0] && $info[1] < $size[1]) {
            $this->image->resize($size[0], $size[1], $bg);
        } else if ($info[0] < $size[0]) {
            $this->image->zoomCrop($info[0], $size[1])->resize($size[0], $size[1], $bg);
        } else if ($info[1] < $size[1]) {
            $this->image->zoomCrop($size[0], $info[1])->resize($size[0], $size[1], $bg);
        } else {
            $this->image->zoomCrop($size[0], $size[1]);
        }
        
        $path_to_image = $this->image->guess(90);
        $image = file_get_contents($path_to_image);
        return $this->responder->raw($image, 200, ['Content-Type' => $info['mime']]);
    }
    
}
