<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Files;

/**
 * Description of EntityFileLocator
 *
 * @author Sonia
 */
class EntityFileLocator
{
    protected $fileManager;
    
    public function __construct(\League\Flysystem\MountManager $fileManager, $baseUrl)
    {
        $this->fileManager = $fileManager;
        $this->baseUrl = $baseUrl;
    }
    
    public function getFile($entity, $field)
    {
        return $this->fileManager->read('uploads://'.$this->getFilePath($entity, $field));
    }

    public function getFileFromPath($path)
    {
        return $this->fileManager->read('uploads://'.$path);
    }
    
    public function getFilePath($entity, $field)
    {
        return $this->getFieldDirPath($entity, $field).'/'.$entity->get($field);
    }
    
    public function getFieldDirPath($entity, $field)
    {
        return $this->getEntityDirPath($entity).'/'.$field;
    }
    
    public function getEntityDirPath($entity)
    {
        return $entity->table().'/'.$entity->primaryKey();
    }
    
    public function getPublicUrl($entity, $field, $format)
    {
        return $this->baseUrl.'/'.$format.'/'.$this->getFilePath($entity, $field);
    }

    public function getPublicUrlFromPath($path, $format)
    {
        return $this->baseUrl.'/'.$format.'/'.$path;
    }
}
