<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Translation;

/**
 *
 * @author Sonia
 */
interface TranslatableEntityInterface
{
    public function getTranslations();
    
    public function setTranslations($data);
}
