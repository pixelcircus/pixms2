<?php

namespace Pixms\Url;

use Symfony\Component\HttpFoundation\Request;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UrlFactory
 *
 * @author Sonia
 */
class UrlFactory
{
    protected $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    public function create($uri, $params = []) {
        $parsed_uri = $this->parseUri($uri, $params);
        return $this->request->getBasePath().$parsed_uri;
    }
    
    public function parseUri($uri, $params = []) {
        $regex = '#\{([0-9a-z_]+):?[a-z]*\}+#';
        $matches = [];
        $params = array_merge($this->request->attributes->all(), $params);
        if (preg_match_all($regex, $uri, $matches)) {
            foreach ($matches[1] as $key => $attr) {
                if (!isset($params[$attr])) {
                    throw new \Exception('Attribute used to generate URLs does not exist: '.$attr);
                } else {
                    $uri = str_replace($matches[0][$key], $params[$attr], $uri);
                }
            }
        }
        
        return $uri;
        
    }
}
