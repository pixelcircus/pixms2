<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Url; 

use Laasti\Notifications\NotificationService;
use Symfony\Component\HttpFoundation\RedirectResponse as BaseRedirectResponse;

/**
 * Description of RedirectResponse
 *
 * @author Sonia
 */
class RedirectSuccessResponse extends BaseRedirectResponse
{
    protected $notification;
    protected $message;
    protected $urlFactory;
    
    public function __construct(UrlFactory $urlFactory, NotificationService $notification, $url, $message = '', $status = 302, $headers = array()) {
        $this->notification = $notification;
        $this->message = $message;
        $this->urlFactory = $urlFactory;
        parent::__construct($url, $status, $headers);
    }
    
    
    public function setTargetUrl($url)
    {   
        $url = $this->urlFactory->create($url);
        parent::setTargetUrl($url);
    }
    public function setMessage($message) {
        $this->message = $message;
        return $this;
    }
    
    public function send() {
        $this->notification->success($this->message);
        return parent::send();
    }
}
