<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Url;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of DataTableController
 *
 * @author Sonia
 */
class RedirectController
{
    
    protected $error;
    protected $success;
    protected $repository;
    
    public function __construct($command, \Symfony\Component\HttpFoundation\RedirectResponse $success, \Symfony\Component\HttpFoundation\RedirectResponse $error)
    {
        $this->command = $command;
        $this->success = $success;
        $this->error = $error;
    }

    public function respond(Request $request)
    {
        $answer = $this->command->run();
        if ($answer['result']) {
            return $this->success;
        } else {
            return $this->error;
        }
    }
}
