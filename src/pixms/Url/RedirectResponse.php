<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Url;

use Symfony\Component\HttpFoundation\RedirectResponse as BaseRedirectResponse; 

/**
 * Description of RedirectResponse
 *
 * @author Sonia
 */
class RedirectResponse extends BaseRedirectResponse
{
    protected static $urlFactory;
    
    
    public function __construct(UrlFactory $urlFactory, $url = '/', $status = 302, $headers = array())
    {
        self::$urlFactory = $urlFactory;
        parent::__construct($url, $status, $headers);
    }
    
    /**
     * {@inheritdoc}
     */
    public static function create($url = '', $status = 302, $headers = array())
    {
        return new static(self::$urlFactory, $url, $status, $headers);
    }
    
    public function setTargetUrl($url)
    {
        $url = self::$urlFactory->create($url);
        parent::setTargetUrl($url);
    }
}
