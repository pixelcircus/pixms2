<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use Pixms\CrudModules\Interfaces\ModuleInterface;

/**
 * Description of AbstractModule
 *
 * @author Sonia
 */
abstract class AbstractModule extends \League\Container\ServiceProvider implements ModuleInterface, \Laasti\Providers\RoutableProviderInterface
{
    protected $baseDir;
    protected $mapper = 'Pixms\Spot\PixmsMapper';
    protected $crudService = 'Pixms\CrudModules\CrudService';
    
    public function register() {
        $c = $this->getContainer();
        $c->addItems($this->getConfig());
        $this->loadLanguageFiles();
    }
    
    protected function loadLanguageFiles() {
        $c = $this->getContainer();
        $t = $c->get('Symfony\Component\Translation\Translator');
        if (is_dir($this->baseDir.'/languages')) {
            $files = array_diff(scandir($this->baseDir.'/languages'), ['.', '..']);
            foreach ($files as $file) {
                if (is_file($this->baseDir.'/languages/'.$file)) {
                    $lang = str_replace('.php', '', $file);
                    $t->addResource('array', require $this->baseDir.'/languages/'.$file, $lang, $this->getIdentifier());
                }
            }
        }
    }
    
    public function getFormFactory() {
        return $this->getIdentifier().':FormFactory';
    }
    
    public function getEntity() {
        return $this->getIdentifier().':Entity';
    }
    
    public function getMapper() {
        return $this->getIdentifier().':Mapper';
    }
    
    public function getDataTableFactory() {
        return $this->getIdentifier().':DataTableFactory';
    }
    
    public function getCrudService() {
        return $this->getIdentifier().':CrudService';
    }
    
    public function getIdentifier() {
        return $this->identifier;
    }
    
    public function getNamespace() {
        $class = get_class($this);
        return substr($class, 0, strrpos($class, '\\'));
    }
    
    public function provides($alias = null) {
        $config = [
            $this->getFormFactory(),
            $this->getEntity(),
            $this->getMapper(),
            $this->getDataTableFactory(),
            $this->getCrudService(),
        ];
        
        if (! is_null($alias)) {
            return (in_array($alias, $config));
        }
        
        return $config;
    }
    
    public function getRoutes() {
        return [
            ['GET', '/{module:(?:'.$this->getIdentifier().')}', 'Pixms\CrudModules\Controllers\ListController::display'],
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/create', 'Pixms\CrudModules\Controllers\CreateController::display'],
            ['POST', '/{module:(?:'.$this->getIdentifier().')}/create', 'Pixms\CrudModules\Controllers\CreateController::submit'],
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{module:(?:'.$this->getIdentifier().')}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/delete/{id:number}', 'Pixms\CrudModules\Controllers\DeleteController::handle'],
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/activate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::activate'],
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/deactivate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::deactivate'],
            ['POST', '/{module:(?:'.$this->getIdentifier().')}/reorder', 'Pixms\CrudModules\Controllers\ReorderController::handle'],
        ];
    }
}
