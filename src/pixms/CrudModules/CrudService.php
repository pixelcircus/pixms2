<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use DateTime;
use InvalidArgumentException;
use League\Flysystem\MountManager;
use Pixms\CrudModules\Decorators\CollectionDecorator;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Files\EntityFileLocator;
use Pixms\Spot\Exceptions\EntityNotFoundException;
use Pixms\Spot\Exceptions\EntityNotSavedException;
use RuntimeException;
use Spot\Relation\BelongsTo;
use Spot\Relation\HasMany;
use Spot\Relation\HasManyThrough;
use Spot\Relation\HasOne;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Description of CrudService
 *
 * @author Sonia
 */
class CrudService
{
    protected $repository;
    protected $decorator;
    protected $fileManager;
    protected $uploadFolder;
    protected $session;
    protected $entityFileLocator;
    protected $requestEntity;

    public function __construct($repository, CollectionDecorator $decorator = null, MountManager $fileManager = null, SessionInterface $session = null, EntityFileLocator $EntityFileLocator = null)
    {
        $this->repository = $repository;
        $this->decorator = $decorator;
        $this->fileManager = $fileManager;
        $this->session = $session;
        $this->entityFileLocator = $EntityFileLocator;
    }

    public function getRequestEntity() {
        return $this->requestEntity;
    }

    public function createFromRequest(Request $request) {
        $module = $request->attributes->get('_module');
        if ($request->getMethod() !== 'POST') {
            return [
                'data' => [],
                'errors' => []
            ];            
        }
        //TODO Change entity should not receive a UploadedFile, just a string
        $data = array_merge($request->attributes->all(), $request->request->all());
        if (isset($data['date_publish'])) {
            $data['date_publish'] = new DateTime($data['date_publish']);
        }
        $entity = $this->repository->build($data);
        $this->requestEntity = $entity;
        //Translate text NULL into real null value
        $relations = $entity->relations($this->repository, $entity);
        foreach ($relations as $name => $relation) {
            if ($relation instanceof BelongsTo) {
                if ($data[$relation->localKey()] === 'NULL') {
                    $entity->set($relation->localKey(), null);
                }
            }
        }
        if (method_exists($entity, 'getOrderableField')) {
            $this->addOrder($entity);
        }
        $request->attributes->set('entity', $entity);

        $this->moveTempFiles($request->files->all(), $entity);

        
        if ($this->repository->validate($entity)) {
            
            
            //TODO Move image upload to here, not the repository
            if (!$this->repository->insert($entity, ['validate' => false, 'strict' => false])) {
                throw new EntityNotSavedException;
            }
            
            $this->saveTempFiles($entity);
            $this->createRelations($entity, $relations);
            $this->repository->save($entity);
            //Move files to upload folder
            //$this->fileManager->write('uploads://'.$module->getIdentifier().'/'.$entity->primaryKey().'/'.$file->getClientName());
            
            return true;
        } else {
            //TODO make it an interface
            return [
                'data' => $this->prepareData($entity),
                'errors' => $entity->errors()
            ];
        }
    }
    
    public function moveTempFiles($files, $entity) {
        $entityConfig = $entity::fields();
        $entityErrors = $entity->errors();
        $entityFiles = array_intersect_key($files, $entityConfig);
        $entityValidFiles = array_diff_key($entityFiles, $entityErrors);
        $tempFiles = $this->session->get('tempfiles', []);
        foreach ($entityValidFiles as $field => $file) {
            
            if(is_null($file)) {
                continue;
            }
            
            $tempFileName = substr(md5($file->getClientOriginalName().$file->getClientSize().time()), 0, 16).'.'.$file->getClientOriginalExtension();
            $stream = fopen($file->getRealPath(), 'r+');
            $this->fileManager->writeStream('temp://'.$tempFileName, $stream);
            fclose($stream);
            $tempFiles[$tempFileName] = $file->getClientOriginalName();
            $this->session->set('tempfiles', $tempFiles);
            $entity->set($field, $tempFileName);
        }
        
        return $this;
    }
    
    public function saveTempFiles($entity) {
        $tempFiles = $this->session->get('tempfiles');
        $fileFields = array_filter($entity::fields(), function($v) { return $v['type'] === 'file';});
        foreach ($fileFields as $field => $config) {
            $tempFile = $entity->get($field);
            if (isset($tempFiles[$tempFile])) {
                $dirPath = $this->entityFileLocator->getFieldDirPath($entity, $field).'/';
                $savePath = 'uploads://'.$dirPath;
                $origFilename = $tempFiles[$entity->get($field)];
                $filename = $origFilename;
                
                while ($this->fileManager->has($savePath.$filename)) {
                    $filename = substr(md5($origFilename.time().rand(0, 50)), 0, 6).'-'.$origFilename;
                }
                //TODO Create upload entry in database
                $entity->set($field, $filename);
                $uploadRepo = $this->repository->getMapper('Pixms\Modules\Uploads\Entities\UploadEntity');
                $uploadRepo->create(['path' => $dirPath.$filename]);
                $this->fileManager->move('temp://'.$tempFile, $savePath.$filename);
            }
        }
        return $this;
    }
    
    public function cleanFiles($entity) {
        $fileFields = array_filter($entity::fields(), function($v) { return $v['type'] === 'file';});
        foreach ($fileFields as $field => $config) {
            $entityFilename = $entity->get($field);
            
            $files = $this->fileManager->listContents('uploads://'.$this->entityFileLocator->getFieldDirPath($entity, $field).'/');
            $uploadRepo = $this->repository->getMapper('Pixms\Modules\Uploads\Entities\UploadEntity');
            foreach ($files as $file) {
                if ($file['basename'] !== $entityFilename) {
                    $uploadRepo->delete(['path :like' => $file['dirname'].'/'.$file['basename']]);
                    $this->fileManager->delete($file['filesystem'].'://'.$file['path']);
                }
            }
        }
        return $this;
    }
    
    public function updateFromRequest(Request $request) {
        $pk = $this->repository->build([])->primaryKeyField();
        $entity = $this->repository->get(urldecode($request->attributes->get($pk)));
        if ($entity === false) {
            throw new EntityNotFoundException();
        }
        $this->requestEntity = $entity;
        
        if ($request->getMethod() !== 'POST') {
            return [
                'data' => $this->prepareData($entity),
                'errors' => []
            ];            
        }
        
        //TODO filter request to prepare data
        //Add EntityFilterableInterface to the controller to filter request using the types in the entity's field
        //Add FilterableInterface to the controller to filter request for the controller's 
        //Nullify empty strings if nullable (not required, and no default)
        //Make it so that you can define child entities, maybe use relations or allow array config with keys
        $data = array_merge($request->request->all(), $request->files->all());
        if (isset($data['date_publish'])) {
            $data['date_publish'] = new DateTime($data['date_publish']);
        }

        //Translate text NULL into real null value
        $relations = $entity->relations($this->repository, $entity);
        foreach ($relations as $name => $relation) {
            if ($relation instanceof BelongsTo) {
                if ($data[$relation->localKey()] === 'NULL') {
                    $data[$relation->localKey()] = null;
                }
            } 
        }

        $entity->data($data);

        if ($this->repository->validate($entity)) {
            
            $this->moveTempFiles($request->files->all(), $entity);
            $this->saveTempFiles($entity);
            $this->cleanFiles($entity);

            $this->createRelations($entity, $relations);
            
            //TODO Move image upload to here, not the repository
            if ($this->repository->update($entity, ['validate' => false, 'strict' => false]) === false) {
                throw new EntityNotSavedException;
            }
            
            
            return true;
        } else {
            
            $this->moveTempFiles($request->files->all(), $entity);
            
            return [
                'data' => $this->prepareData($entity),
                'errors' => $entity->errors()
            ];
        }
    }

    protected function prepareData($entity) {
        return $this->repository->deepData($entity);
    }

    public function createRelations($entity, $relations)
    {
        foreach ($relations as $relation_name => $relation) {

            if ($relation instanceof HasManyThrough) {
                $through_mapper = $this->repository->getMapper($relation->throughEntityName());
                $already_exists = [];
                $current = $entity->get($relation_name);
                if (is_object($current)) {
                    $current = [];
                }
                foreach ($relation as $related) {
                    if (!in_array($related->primaryKey(), $current)) {
                        $condition = [$relation->foreignKey() => $related->primaryKey(), $relation->localKey() => $entity->primaryKey()];
                        $through_mapper->delete($condition);
                    } else {
                        $already_exists[] = $related->primaryKey();
                    }
                }
                $to_add = array_diff($current, $already_exists);
                foreach ($to_add as $id) {
                    $through_mapper->create([$relation->foreignKey() => $id, $relation->localKey() => $entity->primaryKey()]);
                }
                
            }
        }

        return $this;
    }
    
    public function buildDataTable(DataTableFactoryInterface $factory, Request $request) {
        //TODO The data should be retrieved here and not in the factory
        return $factory->create($this->getDataTableData($request));
        
    }
    
    public function getDataTableData($request) {
        //TODO decorate the results here with a class wrapper with a cursor
        $results = $this->repository->all();
        
        //Reorderable entities need to be sorted by priority
        if (method_exists($this->repository->entity(), 'getOrderableField')) {
            $order_field = call_user_func(array($this->repository->entity(), 'getOrderableField'));  
            $results->order([$order_field => 'ASC']);
        }
        return $this->decorator->create($results);
    }
    
    //TODO Move to its own status service
    public function activateEntityById($id) {
        
        $entity = $this->repository->get($id);

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        if ($entity->status !== 1) {
            $entity->status = 1;

            if (!$this->repository->save($entity)) {
                throw new EntityNotSavedException();
            }
            
            return true;
        } else {
            return false;
        }
        
    }
    
    //TODO Move to its own status service
    public function deactivateEntityById($id) {
        
        $entity = $this->repository->get($id);

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        if ($entity->status !== 0) {
            $entity->status = 0;

            if (!$this->repository->save($entity)) {
                throw new EntityNotSavedException();
            }
            
            return true;
        } else {
            return false;
        }
        
    }
    
    public function deleteEntityById($id) {
        
        $this->deleteEntityRelationsById($id);
        $this->deleteEntityFilesById($id);
        
        if (!$this->repository->delete(array('id' => $id))) {
            throw new EntityNotFoundException();
        }
        
        return true;
    }
    
    public function deleteEntityFilesById($id) {
        $entity = $this->repository->get($id);
        if ($entity === false) {
            throw new EntityNotFoundException();
        }
        $uploadRepo = $this->repository->getMapper('Pixms\Modules\Uploads\Entities\UploadEntity');
        $dir = $this->entityFileLocator->getEntityDirPath($entity);
        $uploadRepo->delete(['path :like' => $dir.'%']);
        $this->fileManager->deleteDir('uploads://'.$dir);
        return true;
    }
    
    public function deleteEntityRelationsById($id) {
        $entity = $this->repository->get($id);
        if ($entity === false) {
            throw new EntityNotFoundException();
        }
        $relations = $entity->relations($this->repository, $entity);
        
        foreach ($relations as $relation_name => $relation) {
            
            if ($relation instanceof BelongsTo) {
                //Nothing to do the relation is on itself
            } else if ($relation instanceof HasManyThrough) {
                $through_entity = $relation->throughEntityName();
                $through_mapper = $this->repository->getMapper($through_entity);
                $collection = $through_mapper->where([$relation->localKey() => $entity->primaryKey()]);
                foreach ($collection as $through_entity) {
                    $dir = $this->entityFileLocator->getEntityDirPath($through_entity);
                    $uploadRepo->delete(['path :like' => $dir.'%']);
                    $this->fileManager->deleteDir('uploads://'.$dir);
                }
                $through_mapper->delete([$relation->localKey() => $entity->primaryKey()]);
                //IF table is related to itself delete from foreign key too
                if ($relation->entityName() === get_class($entity)) {
                    $collection = $through_mapper->where([$relation->foreignKey() => $entity->primaryKey()]);
                    foreach ($collection as $through_entity) {
                        $dir = $this->entityFileLocator->getEntityDirPath($through_entity);
                        $uploadRepo->delete(['path :like' => $dir.'%']);
                        $this->fileManager->deleteDir('uploads://'.$dir);
                    }
                    $through_mapper->delete([$relation->foreignKey() => $entity->primaryKey()]);
                }
            } else if ($relation instanceof HasMany) {
                $related_mapper = $this->repository->getMapper($relation->entityName());
                $related_fields = call_user_func([$relation->entityName(), 'fields']);
                $foreign_field = $related_fields[$relation->foreignKey()];
                if (isset($foreign_field['required']) && $foreign_field['required'] === true) {
                    $collection = $related_mapper->where([$relation->foreignKey() => $entity->primaryKey()]);
                    foreach ($collection as $related_entity) {
                        $dir = $this->entityFileLocator->getEntityDirPath($related_entity);
                        $uploadRepo->delete(['path :like' => $dir.'%']);
                        $this->fileManager->deleteDir('uploads://'.$dir);
                    }
                    $related_mapper->delete([$relation->foreignKey() => $entity->primaryKey()]);
                } else {
                    $table = $related_mapper->table();
                    $related_mapper->connection()->exec('UPDATE '.$table.' SET '.$relation->foreignKey().' = NULL');
                }
            } else {
                throw new RuntimeException('Unsupported relation type: '.  get_class($relation));
            }
        }
        
    }
    
    public function reorderEntities($direction, $from, $to) {
        $order_field = call_user_func(array($this->repository->entity(), 'getOrderableField'));
        
        $entity = $this->repository->where([$order_field => $from])->first();

        if ($entity === false) {
            throw new EntityNotFoundException();
        }

        $entity->priority = $to;
        
        $table = $this->repository->table();
        
        if ($direction == 'forward') {
            $result = (bool) $this->repository->connection()->exec("UPDATE $table
                SET `$order_field` = `$order_field`-1
                WHERE `$order_field` > $from AND `$order_field` <= $to");
        } else {
            $result = (bool) $this->repository->connection()->exec("UPDATE $table
                SET `$order_field` = `$order_field`+1
                WHERE `$order_field` >= $to AND `$order_field` < $from");
        }
        
        if ($this->repository->update($entity) === false) {
            throw new EntityNotSavedException();
        }
        
        return $result;
        
    }

    public function getRelationChoices($relation_name, $display_property, $where = [], $order = []) {
        $entity = $this->repository->entity();
        $relations = call_user_func_array([$entity, 'relations'], [$this->repository, $this->repository->build([])]);
        $fields = call_user_func([$entity, 'fields']);
        $choices = [];

        if (!isset($relations[$relation_name])) {
            throw new InvalidArgumentException('The relation "'.$relation_name.'" does not exist for entity: '.$entity);
        }

        $relation = $relations[$relation_name];
        
        if ($relation instanceof BelongsTo) {
            $local_key = $relation->localKey();
            if (!isset($fields[$local_key]['required']) || $fields[$local_key]['required'] === false) {
                $choices['NULL'] = '---';
            }
            $related_mapper = $this->repository->getMapper($relation->entityName());
        } else if ($relation instanceof HasManyThrough) {
            $through_entity = $relation->throughEntityName();
            $through_mapper = $this->repository->getMapper($through_entity);
            $through_relations = call_user_func_array([$through_entity, 'relations'], [$through_mapper, $through_mapper->build([])]);
            foreach ($through_relations as $through_relation) {
                if ($through_relation instanceof HasOne && $through_relation->foreignKey() === $relation->foreignKey()) {
                    $related_mapper = $this->repository->getMapper($through_relation->entityName());
                    break;
                }
            }
            if (!isset($related_mapper)) {
                throw new RuntimeException('Cannot find the relation "'.$relation_name.'" through: '.$through_entity);
            } 
        } else {
            throw new RuntimeException('Unsupported relation type: '.  get_class($relation));
        }
        $results = $related_mapper->where($where)->order($order);
        foreach ($results as $result) {
            $choices[$result->primaryKey()] = $result->get($display_property);
        }
        return $choices;
    }

    public function getRelatedCollection($relation_name, $data, $where = [], $order = []) {
        //Recreate entity from data
        $entity = $this->repository->get($data);
        $relations = call_user_func_array([$entity, 'relations'], [$this->repository, $entity]);

        if (!isset($relations[$relation_name])) {
            throw new InvalidArgumentException('The relation "'.$relation_name.'" does not exist for entity: '.$this->repository->entity());
        }
        $relation = $relations[$relation_name];

        if ($relation instanceof HasMany) {
            $related_mapper = $this->repository->getMapper($relation->entityName());
            $where[$relation->foreignKey()] = $data[$relation->localKey()];
            //TODO Maybe it shouldn't be completely automatic, maybe check if empty
            if (method_exists($related_mapper->entity(), 'getOrderableField')) {
                $order_field = call_user_func(array($related_mapper->entity(), 'getOrderableField'));
                $order = [$order_field => 'ASC'];
            }
            return $related_mapper->where($where)->order($order);
        }

        throw new RuntimeException('Unsupported relation type: '.  get_class($relation));

    }
    
    protected function addOrder($entity) {
        $order_field = $entity->getOrderableField();
        $table = $this->repository->table();

        $result = $this->repository->connection()->fetchAssoc("SELECT $order_field FROM $table ORDER BY $order_field DESC LIMIT 1");
        
        //No results means  it's the first page so we pass 1
        $entity->$order_field = $result === false ? 1 : $result[$order_field] + 1;        
    }

}
