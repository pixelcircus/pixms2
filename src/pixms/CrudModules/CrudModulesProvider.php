<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules;

use League\Container\ServiceProvider;
use Laasti\Providers\RoutableProviderInterface;

/**
 * Description of WhoopsProvider
 *
 * @author Sonia
 */
class CrudModulesProvider extends ServiceProvider implements RoutableProviderInterface
{

    protected $provides = [
        'Pixms\CrudModules\CrudService',
        'Pixms\CrudModules\ModulesLoader',
        'Pixms\CrudModules\Middlewares\ModulesMiddleware',
    ];
    
    protected $defaultConfig = [
        'modules' => [
            'Pixms\Modules\Users\UsersModule',
            'Pixms\Modules\Slides\SlidesModule',
            'Pixms\Modules\News\NewsModule',
            'Pixms\Modules\Uploads\UploadsModule',
            'Pixms\Modules\News\NewsSectionsModule',
        ],
    ];

    protected $modulesLoader;
    protected $config;

    public function register()
    {
        $di= $this->getContainer();

        $config = $this->defaultConfig;
        if (isset($di['CrudModules.config']) && is_array($di['CrudModules.config'])) {
            $config = array_merge($config, $di['CrudModules.config']);
        }
        
        if (!$di->isRegistered('Pixms\CrudModules\CrudService')) {
            $di->add('Pixms\CrudModules\CrudService', 'Pixms\CrudModules\CrudService', true);
        }
        
        $di->add('Pixms\CrudModules\ModulesLoader', $this->getModulesLoader(), true);
        $di->add('Pixms\CrudModules\Middlewares\ModulesMiddleware', null, true)->withArguments(['Pixms\CrudModules\ModulesLoader']);
        
        //TODO should be in its own service providers
        $di->add('Pixms\Files\EntityFileLocator', null, true)->withArguments(['League\Flysystem\MountManager', $config['file_server_url']]);

        //TODO Better way to determine decorators
        $this->container->add('Pixms\CrudModules\Decorators\FileDecorator')->withArguments(['Pixms\Files\EntityFileLocator', 'Pixms\CrudModules\Decorators\DateDecorator']);
        $this->container->add('Pixms\CrudModules\Decorators\DateDecorator')->withArguments(['Pixms\CrudModules\Decorators\PriorityDecorator']);
        $this->container->add('Pixms\CrudModules\Decorators\PriorityDecorator')->withArguments(['Pixms\CrudModules\Decorators\StatusDecorator']);
        $this->container->add('Pixms\CrudModules\Decorators\StatusDecorator')->withArguments(['Pixms\Views\TwigRenderer', 'Pixms\Url\UrlFactory', 'Pixms\CrudModules\Decorators\ChoiceTranslatorDecorator']);
        $this->container->add('Pixms\CrudModules\Decorators\ChoiceTranslatorDecorator')->withArguments(['Symfony\Component\Translation\Translator']);
        $this->container->add('CrudModulesDataTableDecorator', 'Pixms\CrudModules\Decorators\CollectionDecorator')->withArguments(['Pixms\CrudModules\Decorators\FileDecorator', null]);
    }
    
    protected function getConfig() {
        if (is_null($this->config)) {
            $di= $this->getContainer();
            $this->config = $this->defaultConfig;
            if (isset($di['CrudModules.config']) && is_array($di['CrudModules.config'])) {
                $this->config = array_merge($this->config, $di['CrudModules.config']);
            }
        }
        return $this->config;
    }
    
    protected function getModulesLoader() {
        if (is_null($this->modulesLoader)) {
            $this->modulesLoader = new \Pixms\CrudModules\ModulesLoader($this->getContainer(), $this->getConfig()['modules']);
        }
        
        return $this->modulesLoader;
    }

    public function getRoutes() {
        //Make sure all modules are loaded, so that they can override the default routes below
        $this->getModulesLoader();
        //TODO Should be in each module for easy override
        return [];
        /*
        return [
            ['GET', '/{module:word}', 'Pixms\CrudModules\Controllers\ListController::display'],
            ['GET', '/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::display'],
            ['POST', '/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::submit'],
            ['GET', '/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
            ['GET', '/{module:word}/delete/{id:number}', 'Pixms\CrudModules\Controllers\DeleteController::handle'],
            ['GET', '/{module:word}/activate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::activate'],
            ['GET', '/{module:word}/deactivate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::deactivate'],
            ['POST', '/{module:word}/reorder', 'Pixms\CrudModules\Controllers\ReorderController::handle'],

            ['GET', '/{parent_module:word}/{parent_id:number}/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::display'],
            ['POST', '/{parent_module:word}/{parent_id:number}/{module:word}/create', 'Pixms\CrudModules\Controllers\CreateController::submit'],
            ['GET', '/{parent_module:word}/{parent_id:number}/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{parent_module:word}/{parent_id:number}/{module:word}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
            ['GET', '/{parent_module:word}/{parent_id:number}/{module:word}/delete/{id:number}', 'Pixms\CrudModules\Controllers\DeleteController::handle'],
            ['GET', '/{parent_module:word}/{parent_id:number}/{module:word}/activate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::activate'],
            ['GET', '/{parent_module:word}/{parent_id:number}/{module:word}/deactivate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::deactivate'],
            ['POST', '/{parent_module:word}/{parent_id:number}/{module:word}/reorder', 'Pixms\CrudModules\Controllers\ReorderController::handle'],
        ];
         * 
         */
    }

}
