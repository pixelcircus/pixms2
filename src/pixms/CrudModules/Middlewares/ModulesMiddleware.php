<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Middlewares;

use Laasti\Stack\MiddlewareInterface;
use Pixms\CrudModules\ModulesLoader;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ModulesMiddleware
 *
 * @author Sonia
 */
class ModulesMiddleware implements MiddlewareInterface
{
    protected $loader;
    protected $routes;
    
    public function __construct(ModulesLoader $loader) {
        $this->loader = $loader;
    }
    
    public function handle(Request $request) {        
        
        if ($request->attributes->get('module', false)) {
            $request->attributes->set('_module', $this->loader->getModule($request->attributes->get('module')));
        }
        
        return $request;
    }
}
