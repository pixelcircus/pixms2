<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Components\Reorder;

/**
 * Description of ReorderableEntityTrait
 *
 * @author Sonia
 */
trait OrderableEntityTrait
{    
    
    public static function getOrderableField() {
        if (self::$orderableField === '') {
            throw new \InvalidArgumentException('Looks like you forgot to set the $reorderableField property in your entity.');
        }
        return self::$orderableField;
    }
    
}
