<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

use Pixms\Files\EntityFileLocator;

/**
 * Description of StatusDecorator
 *
 * @author Sonia
 */
class FileDecorator extends AbstractDecorator
{
    protected $twig;
    protected $entityFileLocator;
    
    public function __construct(EntityFileLocator $entityFileLocator, $object = null)
    {
        $this->object = $object;
        $this->entityFileLocator = $entityFileLocator;
    }
    
    public function __call($method, $args) {
        $entity = $this->getOriginalObject();
        $fields = $entity->fields();
        
        if (isset($fields[$method]) && $fields[$method]['type'] === 'file') {
            //TODO move to template
            return '<a href="'.$this->entityFileLocator->getPublicUrl($entity, $method, 'maxwidth/1024').'" class="image-popup"><span class="icon-pictures" title="See picture"></span></a>';
        }
        
        return parent::__call($method, $args);
    }
    
}
