<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;


/**
 * Description of StatusDecorator
 *
 * @author Sonia
 */
class ChoiceTranslatorDecorator extends AbstractDecorator
{
    protected $twig;
    protected $trans;
    
    public function __construct(\Symfony\Component\Translation\Translator $trans, $object = null)
    {
        $this->object = $object;
        $this->trans = $trans;
    }
    
    public function __call($method, $args) {
        $entity = $this->getOriginalObject();
        $fields = $entity->fields();
        
        if (isset($fields[$method]) && isset($fields[$method]['choices'])) {
            $key = 'choices_'.$method.'_'.$entity->get($method);
            if ($this->trans->trans($key) !== $key) {
                return $this->trans->trans($key);
                //TODO shoudn't assume table equals identifier
            } else if ($this->trans->trans($key, [], $entity::table()) !== $key) {
                return $this->trans->trans($key, [], $entity::table());
            }
            
            return $entity->get($method);
        }
        
        return parent::__call($method, $args);
    }
    
}
