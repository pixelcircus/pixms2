<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

/**
 * Description of StatusDecorator
 *
 * @author Sonia
 */
class DateDecorator extends AbstractDecorator
{
    
    public function __construct($object = null)
    {
        $this->object = $object;
    }
    
    public function __call($method, $args) {
        $fields = $this->getOriginalObject()->fields();
        
        if (isset($fields[$method]) && $fields[$method]['type'] === 'date') {
            return $this->get($method)->format('Y-m-d');
        }
        
        return parent::__call($method, $args);
    }
    
    
}
