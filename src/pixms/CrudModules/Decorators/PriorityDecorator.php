<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Decorators;

/**
 * Description of StatusDecorator
 *
 * @author Sonia
 */
class PriorityDecorator extends AbstractDecorator
{
    
    public function __construct($object = null)
    {
        $this->object = $object;
    }
    
    public function priority()
    {
        $entity = $this->getOriginalObject();
        return '<span class="icon-move-vertical" data-' . $entity->orderableField . '="' . $this->get($entity->orderableField) . '"></span>';
    }
}