<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Interfaces;

/**
 *
 * @author Sonia
 */
interface ModuleInterface
{
    public function register();
    public function getIdentifier();
    public function getFormFactory();
    public function getEntity();
    public function getMapper();
    public function getDataTableFactory();
    public function getCrudService();
    public function getNamespace();
    public function getConfig();
}
