<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class ReorderController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, NotificationService $notification, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function handle(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $post = $request->request;
        
        $crudService = $module->getCrudService($request);

        //TODO shouldn't access the container from the controller
        $service = $this->container->get($crudService);
        $result = $service->reorderEntities($post->get('direction'), $post->get('from'), $post->get('to'));

        $this->responder->clearData();
        $this->responder->setData('result', $result);

        return $this->responder->json();
    }


    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.update',
        ];
    }
}
