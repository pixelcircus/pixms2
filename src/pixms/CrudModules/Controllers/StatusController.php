<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class StatusController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, NotificationService $notification, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function activate(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        
        $crudService = $module->getCrudService($request);
        $trans = $this->container->get('Symfony\Component\Translation\Translator');

        //TODO shouldn't access the container from the controller
        $service = $this->container->get($crudService);
        if ($service->activateEntityById($request->attributes->get('id'))) {
            $this->notification->success($trans->trans('callback_activated', [], $id));
        } else {
            $this->notification->error($trans->trans('callback_already_activated', [], $id));
        }

        if ($request->query->get('back_url', false)) {
            return $this->responder->redirect($request->query->get('back_url'));
        } else if ($request->attributes->get('parent_module', false)) {
            return $this->responder->redirect('/'.$request->attributes->get('parent_module').'/details/'.$request->attributes->get('parent_id'));
        }
        return $this->responder->redirect('/'.$id);
    }

    public function deactivate(Request $request)
    {
        $module = $request->attributes->get('_module');

        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);

        $crudService = $module->getCrudService($request);
        $trans = $this->container->get('Symfony\Component\Translation\Translator');

        //TODO shouldn't access the container from the controller
        $service = $this->container->get($crudService);
        if ($service->deactivateEntityById($request->attributes->get('id'))) {
            $this->notification->success($trans->trans('callback_deactivated', [], $id));
        } else {
            $this->notification->error($trans->trans('callback_already_deactivated', [], $id));
        }

        if ($request->query->get('back_url', false)) {
            return $this->responder->redirect($request->query->get('back_url'));
        } else if ($request->attributes->get('parent_module', false)) {
            return $this->responder->redirect('/'.$request->attributes->get('parent_module').'/details/'.$request->attributes->get('parent_id'));
        }
        return $this->responder->redirect('/'.$id);
    }


    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.update',
        ];
    }
}
