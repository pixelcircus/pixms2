<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class ListController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
    }

    public function display(Request $request)
    {
        $module = $request->attributes->get('_module');
        $trans = $this->container->get('Symfony\Component\Translation\Translator');
        
        $id = $module->getIdentifier();

        $dataTable = $module->getDataTableFactory($request);
        $service = $this->container->get($module->getCrudService());
        
        $this->responder->setData('page_title', $trans->trans('title_list', [], $id));
        $this->responder->setData('links', [
            [
                'label' => $trans->trans('button_create', [], $id),
                'classes' => ['button', 'ok'],
                'url' => $this->url->create('/'.$id.'/create')
            ]
        ]);
        

        $this->responder->setData('datatable', $service->buildDataTable($this->container->get($dataTable), $request));

        return $this->responder->view('datatables.twig');
    }



    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.view',
        ];
    }

}
