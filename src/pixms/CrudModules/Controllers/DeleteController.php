<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Spot\Exceptions\EntityNotFoundException;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class DeleteController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, NotificationService $notification, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->container = $container;
        $this->notification = $notification;
    }

    public function handle(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $single = substr($id, 0, strlen($id)-1);

        $crudService = $module->getCrudService($request);
        $trans = $this->container->get('Symfony\Component\Translation\Translator');
        //TODO shouldn't access the container from the controller        
        $service = $this->container->get($crudService);
        try {
            $service->deleteEntityById($request->attributes->get('id'));
            $this->notification->success($trans->trans('callback_deleted', [], $id));
        } catch (EntityNotFoundException $e) {
            $this->notification->error($trans->trans('callback_not_exists', [], $id));
        }
        
        if ($request->query->get('back_url', false)) {
            return $this->responder->redirect($request->query->get('back_url'));
        } else if ($request->attributes->get('parent_module', false)) {
            return $this->responder->redirect('/'.$request->attributes->get('parent_module').'/details/'.$request->attributes->get('parent_id'));
        }

        return $this->responder->redirect('/'.$id);
    }

    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.delete',
        ];
    }
    

}
