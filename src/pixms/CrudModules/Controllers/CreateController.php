<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class CreateController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, NotificationService $notification, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->notification = $notification;
        $this->container = $container;
    }

    public function display(Request $request)
    {
        $module = $request->attributes->get('_module');
        $trans = $this->container->get('Symfony\Component\Translation\Translator');
        
        $id = $module->getIdentifier();
        //TODO shouldn't access the container from the controller        
        //$this->container->add($crudService)->withArguments([$mapper, null, 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator'], true);
        $formFactory = $this->container->get($module->getFormFactory());
        
        $this->responder->setData('page_title', $trans->trans('title_create', [], $id));

        $this->responder->setData('form', $formFactory->create('', []));
        
        return $this->responder->view('form.twig');
    }

    public function submit($request) {

        $module = $request->attributes->get('_module');

        $id = $module->getIdentifier();
        $crudService = $module->getCrudService($request);
        $service = $this->container->get($crudService);
        $formFactory = $this->container->get($module->getFormFactory($request));

        $payload = $service->createFromRequest($request);
        $trans = $this->container->get('Symfony\Component\Translation\Translator');

        if ($payload === true) {
            $this->notification->success($trans->trans('callback_created', [], $id));
            if ($request->query->get('back_url', false)) {
                return $this->responder->redirect($request->query->get('back_url'));
            } else if ($request->attributes->get('parent_module', false)) {
                return $this->responder->redirect('/'.$request->attributes->get('parent_module').'/details/'.$request->attributes->get('parent_id'));
            }
            return $this->responder->redirect('/'.$id);
        }
        
        $this->responder->setData('page_title', $trans->trans('title_create', [], $id));

        $this->responder->setData('form', $formFactory->create('', $payload['data'], $payload['errors']));


        return $this->responder->view('form.twig');
    }

    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.create',
        ];
    }
    
}
