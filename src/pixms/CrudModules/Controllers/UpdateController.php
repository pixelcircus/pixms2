<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\CrudModules\Controllers;

use Laasti\Notifications\NotificationService;
use Laasti\Response\ResponderInterface;
use League\Container\ContainerInterface;
use Pixms\AuthSquared\Interfaces\AuthorizableInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class UpdateController implements AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;
    protected $url = null;
    protected $container = null;
    protected $notification = null;

    
    public function __construct(ResponderInterface $responder, UrlFactory $url, NotificationService $notification, ContainerInterface $container)
    {
        $this->responder = $responder;
        $this->url = $url;
        $this->notification = $notification;
        $this->container = $container;
    }

    public function display(Request $request)
    {
        $module = $request->attributes->get('_module');
        
        $id = $module->getIdentifier();
        $trans = $this->container->get('Symfony\Component\Translation\Translator');
        
        $crudService = $module->getCrudService($request);
         //TODO shouldn't access the container from the controller
        $service = $this->container->get($crudService);
        $payload = $service->updateFromRequest($request);
        $formFactory = $this->container->get($module->getFormFactory($request));
        
        $this->responder->setData('page_title', $trans->trans('title_update', [], $id));

        $this->responder->setData('form', $formFactory->create('', $payload['data']));

        return $this->responder->view('form.twig');
    }

    public function submit($request) {

        $module = $request->attributes->get('_module');

        $id = $module->getIdentifier();

        $crudService = $module->getCrudService($request);

        $service = $this->container->get($crudService);
        $formFactory = $this->container->get($module->getFormFactory($request));
        $payload = $service->updateFromRequest($request);
        $trans = $this->container->get('Symfony\Component\Translation\Translator');

        if ($payload === true) {
            $this->notification->success($trans->trans('callback_updated', [], $id));

            if ($request->query->get('back_url', false)) {
                return $this->responder->redirect($request->query->get('back_url'));
            } else if ($request->attributes->get('parent_module', false)) {
                return $this->responder->redirect('/'.$request->attributes->get('parent_module').'/details/'.$request->attributes->get('parent_id'));
            }
            
            return $this->responder->redirect('/'.$id);
        }
        
        $this->responder->setData('page_title', $trans->trans('title_update', [], $id));

        $this->responder->setData('form', $formFactory->create('', $payload['data'], $payload['errors']));


        return $this->responder->view('form.twig');
    }

    public function getRights(Request $request)
    {
        return [
            'pixms.access', 'pixms.update',
        ];
    }
    
}
