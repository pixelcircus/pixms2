<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core\Providers;

use League\Container\ServiceProvider;

/**
 * Description of TwigProvider
 *
 * @author Sonia
 */
class TwigServiceProvider extends ServiceProvider
{

    protected $provides = [
        'Twig_Environment',
        'Twig_LoaderInterface'
    ];
    
    protected $defaultConfig = [
        
        /**
         * An absolute path to the location of the templates
         * @var string
         */
        'templates_locations' => [],
        /**
         * When set to true, the generated templates have a __toString() method that you can use to display the generated nodes 
         * @var Boolean 
         */
        'debug' => false,

        /**
         * The charset used by the templates
         * @var string
         */
        'charset' => 'utf-8',

        /**
         * The base template class to use for generated templates
         * @var string
         */
        'template_class' => 'Twig_Template',

        /**
         * An absolute path where to store the compiled templates, or false to disable caching
         * @var Boolean|string
         */
        'cache' => false,

        /**
         * When developing with Twig, it's useful to recompile the template whenever the source code changes.
         * If you don't provide a value for the auto_reload option, it will be determined automatically based on the debug value.
         * @var Boolean
         */
        'auto_reload' => null,

        /**
         *  If set to false, Twig will silently ignore invalid variables (variables and or attributes/methods that do not exist)
         * and replace them with a null value. When set to true, Twig throws an exception instead.
         * @var Boolean
         */
        'strict_variables' => false,

        /**
         * If set to true, HTML auto-escaping will be enabled by default for all templates
         * You can set the escaping strategy to use (css, url, html_attr,
         * or a PHP callback that takes the template "filename" and must return the escaping strategy to use --
         * the callback cannot be a function name to avoid collision with built-in escaping strategies)
         * @var type
         */
        'autoescape' => true,

        /**
         * A flag that indicates which optimizations to apply (default to -1 -- all optimizations are enabled; set it to 0 to disable).
         * @var int
         */
        'optimizations' => -1
    ];

    public function register()
    {
        $c = $this->getContainer();
        $config = $this->defaultConfig;
        
        if (isset($c['Twig.config'])) {
            $config = array_merge($config, $c['Twig.config']);
        }
        
        if (empty($config['templates_locations'])) {
            throw new \RuntimeException('You must provide the path(s) to your templates.');
        }

        if (!$c->isRegistered('Twig_LoaderInterface') && !$c->isSingleton('Twig_LoaderInterface')) {
            $c->add('Twig_LoaderInterface', function() use ($config) {
                $loader = new \Twig_Loader_Filesystem($config['templates_locations']);
                return $loader;
            });
        }

        $c->add('Twig_Environment', function() use ($c, $config) {
            $loader = $c->get('Twig_LoaderInterface');
            
            $twig = new \Twig_Environment($loader, $config);

            if ($c->isInServiceProvider('Symfony\Component\Translation\Translator') || $c->isRegistered('Symfony\Component\Translation\Translator')) {
                $translator = $c->get('Symfony\Component\Translation\Translator');
                $filter = new \Twig_SimpleFilter('trans', function ($string) use ($translator) {
                    return $translator->trans($string);
                });
                $twig->addFilter($filter);
            }
            return $twig;
        }, true);
    }

}
