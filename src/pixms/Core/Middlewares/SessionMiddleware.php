<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core\Middlewares;

use Laasti\Stack\MiddlewareInterface;
use Laasti\Stack\MiddlewareTerminableInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Description of AuthenticationMiddleware
 *
 * @author Sonia
 */
class SessionMiddleware implements MiddlewareInterface, MiddlewareTerminableInterface
{
    
    /**
     *
     * @var SessionInterface 
     */
    protected $session;
    
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }
    
    
    public function handle(Request $request)
    {
        $this->session->start();

        if ($this->session->getMetadataBag()->getCreated()+60*2 < time()) {
            $this->session->migrate(true);
        }

        $lastActivity = $this->session->getMetadataBag()->getLastUsed();

        if ($lastActivity+60*30 < time()) {
            $this->session->invalidate();
        }

        return $request;
    }
    
    public function terminate(Request $request, Response $response)
    {
        $this->session->save();    
    }
}
