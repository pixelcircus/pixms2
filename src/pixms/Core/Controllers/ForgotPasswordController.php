<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class ForgotPasswordController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $renderer = null;
    protected $notification = null;
    protected $repository = null;
    protected $mailer = null;
    protected $trans = null;

    
    public function __construct(ResponderInterface $responder, \Symfony\Component\Translation\Translator $trans, \Pixms\Modules\Users\UserMapper $repository, \Laasti\Notifications\NotificationService $notification, \Laasti\Mailer\Mailer $mailer)
    {
        $this->responder = $responder;
        $this->repository = $repository;
        $this->notification = $notification;
        $this->trans = $trans;
        $this->mailer = $mailer;
    }

    public function handle(Request $request)
    {
        //TODO: Use the form builder
        $this->responder->addData(array(
            'page_title' => $this->trans->trans('auth:title_forgot_password'),
            'layout' => 'limited.twig',
            'members' => array(),
            'form' => array(
                'sections' => array(
                    array(
                        'items' => array(
                            array(
                                "type" => "title",
                                "label" => $this->trans->trans('auth:title_forgot_password')
                            ),
                            array(
                                "type" => "note",
                                "label" => $this->trans->trans('auth:description_forgot_password')
                            ),
                            array(
                                "type" => "text",
                                "label" => $this->trans->trans('auth:field_email'),
                                "name" => "email",
                                "id" => "email-1",
                                "classes" => [],
                                "attributes" => []
                            ),
                            array(
                                "type" => "submit",
                                "label" => $this->trans->trans('auth:submit_send'),
                            ),
                            array(
                                "type" => "link",
                                "url" => $request->getBaseUrl()."/login",
                                "label" => $this->trans->trans('auth:link_return_login'),
                            ),
                        ),
                 ),
                ),
            ),
        ));
        return $this->responder->view('form.twig');
    }

    public function submit(Request $request)
    {
        $entity = $this->repository->build($request->request->all());
        $this->repository->validate($entity);
        $v = new \Valitron\Validator($request->request->all());
        $v->rule('required', ['email']);
        $v->rule('email', 'email');
        if ($v->validate()) {
            $entity = $this->repository->where(['email' => $request->request->get('email')])->first();
            
            if ($entity == false) {
                $this->notification->error('Aucun compte ne correspond à cette adresse courriel.');
                return $this->responder->redirect($request->getPathInfo());
            }
            
            $token = md5($entity->primaryKey().$entity->get('email').time());
            $entity->set('token', $token);
            $entity->set('token_expiration', new \DateTime('+1 hour'));
            $this->repository->save($entity);
            
            $message = new \Laasti\Mailer\Message();
            $message->setSubject('Demande de réinitialisation de mot de passe');
            $message->addTo($entity->get('name'), $entity->get('email'));
            $message->setFakeFrom('Pixms', 'info@pixelcircus.ca');
            //TODO Support for https
            $message->setBody('Voici le lien de réinitialisation de mot de passe: '.'http://'.$request->getHttpHost().$request->getBaseUrl().'/forgot-password/'.$token);
            //$this->mailer->setTextBody('Voici le lien de réinitialisation de mot de passe: '.$request->getBaseUrl().'/forgot-password/'.$token);
            $this->mailer->send($message);
            
            $this->notification->success('Un courriel a été envoyé à votre messagerie.');
            return $this->responder->redirect($request->getPathInfo());
            
        } else {
            $this->notification->error('Adresse courriel non valide.');
            return $this->responder->redirect($request->getPathInfo());
        }
    }
    
    public function reset_form(Request $request, $entity = null) 
    {
        $user = is_null($entity) ? $this->repository->where(['token' => $request->attributes->get('token')])->first() : $entity;
        if ($user != false && $user->token_expiration > new \DateTime()) {
            $errors = $user->errors();
             $this->responder->addData(array(
                'page_title' => 'Réinitialisation du mot de passe',
                'layout' => 'limited.twig',
                'members' => array(),
                'form' => array(
                    'sections' => array(
                        array(
                            'items' => array(
                                array(
                                    "type" => "title",
                                    "label" => "Réinitialisation du mot de passe"
                                ),
                                array(
                                    "type" => "note",
                                    "label" => "Veuillez entrer votre nouveau mot de passe."
                                ),
                                array(
                                    "type" => "password",
                                    "label" => "Mot de passe",
                                    "name" => "password",
                                    "classes" => [],
                                    "errors" => isset($errors['password']) ? $errors['password'] : [],
                                    "attributes" => []
                                ),
                                array(
                                    "type" => "password",
                                    "label" => "Confirmation",
                                    "name" => "password_confirm",
                                    "errors" => isset($errors['password']) ? $errors['password'] : [],
                                    "classes" => [],
                                    "attributes" => []
                                ),
                                array(
                                    "type" => "submit",
                                    "label" => "Envoyer"
                                ),
                                array(
                                    "type" => "link",
                                    "url" => $request->getBaseUrl()."/login",
                                    "label" => "Retour à la page de connexion"
                                ),
                            ),
                     ),
                ),
            ),
        ));
        return $this->responder->view('form.twig');
            
        } else {
            $this->notification->error('La demande a expiré, veuillez refaire une nouvelle demande.');
            return $this->responder->redirect('/forgot-password');
        }
    }
    
    public function reset_password(Request $request) {
        $user = $this->repository->where(['token' => $request->attributes->get('token')])->first();
        if ($user != false && $user->token_expiration > new \DateTime()) {
            $user->data($request->request->all());
            if ($this->repository->validate($user)) {
                $this->repository->save($user);
                $this->notification->success('Votre mot de passe a été réinitialisé, vous pouvez vous connecter.');
                return $this->responder->redirect('/login');
            } else {
                return $this->reset_form($request, $user);
            }
        } else {
            $this->notification->error('La demande a expiré, veuillez refaire une nouvelle demande.');
            return $this->responder->redirect('/forgot-password');
        }
    }

}
