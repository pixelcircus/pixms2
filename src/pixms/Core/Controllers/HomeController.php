<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core\Controllers;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class HomeController implements \Pixms\AuthSquared\Interfaces\AuthorizableInterface
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $responder = null;

    public function __construct(\Laasti\Response\ResponderInterface $responder)
    {
        $this->responder = $responder;
    }

    public function handle(Request $request)
    {        
        $this->responder->setData('content', 'Some page');

        return $this->responder->view('content.twig');
    }
    public function handle2(Request $request)
    {
        $this->responder->setData('content', 'Some pag2e');

        return $this->responder->view('content.twig');
    }


    public function getRights(Request $request)
    {
        return [
            'pixms.access',
        ];
    }
}
