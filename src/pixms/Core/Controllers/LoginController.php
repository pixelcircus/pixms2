<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core\Controllers;

use Laasti\Response\ResponderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of HelloWorld
 *
 * @author Sonia
 */
class LoginController
{

    /**
     *
     * @var \Laasti\TwigRenderer
     */
    protected $renderer = null;
    protected $trans = null;

    
    public function __construct(ResponderInterface $responder, \Symfony\Component\Translation\Translator $trans)
    {
        $this->responder = $responder;
        $this->trans = $trans;
    }

    public function handle(Request $request)
    {
        //TODO: Use the form builder
        $this->responder->addData(array(
            'page_title' => $this->trans->trans('auth:title_login'),
            'layout' => 'limited.twig',
            'members' => array(),
            'form' => array(
                'sections' => array(
                    array(
                        'items' => array(
                            array(
                                "type" => "title",
                                "label" => $this->trans->trans('auth:title_login')
                            ),
                            array(
                                "type" => "text",
                                "label" => $this->trans->trans('auth:field_email'),
                                "name" => "email",
                                "id" => "email-1",
                                "classes" => [],
                                "attributes" => []
                            ),
                            array(
                                "type" => "password",
                                "label" => $this->trans->trans('auth:field_password'),
                                "name" => "password",
                                "id" => "password-1",
                                "classes" => [],
                                "attributes" => []
                            ),
                            array(
                                "type" => "submit",
                                "label" => $this->trans->trans('auth:submit_login')
                            ),
                            array(
                                "type" => "link",
                                "url" => $request->getBaseUrl()."/forgot-password",
                                "label" => $this->trans->trans('auth:link_forgot_password')
                            ),
                        ),
                 ),
                ),
            ),
        ));
        return $this->responder->view('form.twig');
    }

    public function submit(Request $request)
    {
        return $this->handle($request);
    }

}
