<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core;

/**
 * Description of Command
 *
 * @author Sonia
 */
class Command
{
    protected $service;
    protected $method;
    protected $arguments;
    
    public function __construct($service, $method, $arguments = [])
    {
        $this->service = $service;
        $this->method = $method;
        $this->arguments = $arguments;
    }

    public function run() {
        return call_user_func_array([$this->service, $this->method], $this->arguments);
    }
}
