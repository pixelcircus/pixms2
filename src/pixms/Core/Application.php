<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Core;

use Laasti\Application as BaseApplication;
use Valitron\Validator;

/**
 * Description of Application
 *
 * @author Sonia
 */
class Application extends BaseApplication
{


    public function __construct($config = [], $factory = null)
    {
        $default_config = require(__DIR__.'/../config.php');
        $config = array_merge_recursive($default_config, $config);
        parent::__construct($config, $factory);
        
        //TODO shouldn't be here
        $validator = $this->get('Pixms\Files\FileValidator');
        Validator::addRule('image', array($validator, 'isImage'), 'Not an image.');
        Validator::addRule('minwidth', array($validator, 'imageMinWidth'), 'Image too small');
        Validator::addRule('minheight', array($validator, 'imageMinHeight'), 'image too small');
    }
    
    public function getRoutes()
    {
        if (is_null($this->routes)) {
            $this->add('League\Route\RouteCollection', null, true)->withArgument($this);
            $this->routes = $this->get('League\Route\RouteCollection');
            $this->routes->setStrategy($this->get('Laasti\Route\Strategies\TwoStepControllerStrategy'));
            //TODO Make it easier to add patterns
            $this->routes->addPatternMatcher('any', '.+');
            $this->routes->addPatternMatcher('hex', '[0-9a-f]+');
            $this->routes->addPatternMatcher('path', '(?:.*\/.*)?');
            $this->addRoutesFromConfig($this->config['routes']);
        }
        
        return $this->routes;
    }
    
    public function addItems($items) {
        $this->addItemsFromConfig(['di' => $items]);
        
        return $this;
    }

}
