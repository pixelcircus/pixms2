<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Spot\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Description of FileType
 *
 * @author Sonia
 */
class FilesType extends Type
{
    const FILETYPE = 'files'; 
    
    protected static $filePath;

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if (empty($value)) {
            return [];
        }
        
        return explode('|', $value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (is_array($value)) {
             return implode('|', $value);
        } else {
            return null;
        }
    }

    public function getName()
    {
        return self::FILETYPE;
    }
    
    public static function setFilePath($path) {
        self::$filePath = $path;
    }
}
