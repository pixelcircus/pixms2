<?php


namespace Pixms\Spot;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Pixms\Spot\Entity\Manager;
use Spot\EntityInterface;
use Spot\Locator;
use Spot\Mapper;
use Spot\Query\Resolver;
use Spot\Relation\HasMany;
use Symfony\Component\HttpFoundation\Request;
use Valitron\Validator;
/**
 * Description of PixmsMapper
 *
 * @author Sonia
 */
class PixmsMapper extends Mapper
{

    protected $entityName;

    public function __construct(Locator $locator, $entityName = null)
    {
        if (is_object($entityName)) {
            $entityName = get_class($entityName);
        }
        parent::__construct($locator, $entityName ? : $this->entityName);
        $this->migrate();
    }
    
    //SMA NEW FUNCTION
    public function deepData($entity) {
        $relations = $entity->relations($this, $entity);
        $data = $entity->data();
        foreach ($relations as $relation_name => $relation) {
            $data[$relation_name] = [];
            if ($relation instanceof HasMany || $relation instanceof \Spot\Relation\HasManyThrough) {
                $data[$relation_name] = [];
                foreach ($relation as $related) {
                    $data[$relation_name][] = $related->primaryKey();
                }
            } elseif ($relation instanceof \Spot\Relation\BelongsTo || $relation instanceof \Spot\Relation\HasOne) {

                if ($relation->entity() !== false) {
                    $data[$relation_name] = $relation->entity()->primaryKey();
                }
            }
        }
        
        return $data;
    }
    /**
     * Entity manager class for storing information and meta-data about entities
     */
    //SMA Modified to be able to set my own Manager
    public function entityManager()
    {
        $entityName = $this->entity();
        if (!isset(self::$entityManager[$entityName])) {
            self::$entityManager[$entityName] = new Manager($entityName);
        }

        return self::$entityManager[$entityName];
    }

    //SMA NEW METHOD, MAYBE IT SHOULD BE DELETED
    public function fill(EntityInterface $entity, Request $request)
    {

        $files = $request->files->all();
        $post = $request->request->all();
        foreach ($entity->fields() as $field => $config) {
            if (isset($files[$field])) {
                if (!is_null($files[$field])) {
                    $entity->set($field, $files[$field]);
                }
            } else if (isset($post[$field])) {
                $entity->set($field, $post[$field]);
            }
        }

        //TODO: Would be better to check for interface
        $relations = $entity->relations($this, $entity);
        foreach ($relations as $relation_name => $relation) {
            if (isset($post[$relation_name])) {
                $setter = 'set' . ucfirst($relation_name);
                if ($relation instanceof HasMany) {
                    $entity->$setter($post[$relation_name]);
                } else {
                    //TODO Check if other relations need to validated
                }
            }
        }

        return $entity;
    }

    //SMA ADDED upload + relations
    public function insert($entity, array $options = ['strict' => false])
    {
        //$this->uploadFiles($entity);
        
        $result = parent::insert($entity, $options);

      //  $this->saveRelations($entity);

        return $result;
    }

    //SMA ADDED upload + relations
    public function update(EntityInterface $entity, array $options = ['strict' => false])
    {
        //$this->uploadFiles($entity);
        //$this->saveRelations($entity);
        return parent::update($entity, $options);
    }

    //SMA ADDED METHOD
    public function saveRelations(EntityInterface $entity) {
        $relations = $entity->relations($this, $entity);
        //TODO Should have a config to save only loaded relations
        foreach ($relations as $relation_name => $relation) {
            $getter = 'get' . ucfirst($relation_name);
            if ($relation instanceof HasMany) {
                foreach ($entity->$getter() as $child) {
                    $child->set($relation->foreignKey(), $entity->primaryKey());
                    //TODO Error check
                    $this->getMapper(get_class($child))->save($child, []);
                }
            } else {
                //TODO Check if other relations need to validated
            }
        }
        return;
    }
    
    //SMA Changed default options
    public function save(EntityInterface $entity, array $options = ['strict' => false])
    {
        return parent::save($entity, $options);
    }
    

    /**
     * Query resolver class for perparing and executing queries, then returning the results
     */
    //SMA Changed for my own resolver
    public function resolver()
    {
        return new Resolver($this);
    }

    /**
     * Run set validation rules on fields
     */
    public function validate(EntityInterface $entity)
    {
        $v = new Validator($entity->data());
        // Run beforeValidate to know whether or not we can continue
        if (false === $this->eventEmitter()->emit('beforeValidate', [$entity, $this, $v])) {
            return false;
        }

        // Check validation rules on each feild
        $uniqueWhere = [];
        foreach ($this->fields() as $field => $fieldAttrs) {
            // Required field
            if (
            // Explicitly required
                    ( isset($fieldAttrs['required']) && true === $fieldAttrs['required'] )
                    // Primary key without autoincrement
                    || ($fieldAttrs['primary'] === true && $fieldAttrs['autoincrement'] === false)
            ) {
                $v->rule('required', $field);
            }

            // Unique field
            //SMA Removed && $entity->$field !== null
            if ($entity->isNew() && isset($fieldAttrs['unique']) && !empty($fieldAttrs['unique'])) {
                if (is_string($fieldAttrs['unique'])) {
                    // Named group
                    $fieldKeyName = $fieldAttrs['unique'];
                    $uniqueWhere[$fieldKeyName][$field] = $entity->$field;
                } else {
                    $uniqueWhere[$field] = $entity->$field;
                }
            }

            // Run only if field required
            //SMA Changed && for ||
            if ($entity->$field !== null || $fieldAttrs['required'] === true) {
                // Field with 'options'
                if (isset($fieldAttrs['options']) && is_array($fieldAttrs['options'])) {
                    $v->rule('in', $field, $fieldAttrs['options']);
                }

                // Valitron validation rules
                if (isset($fieldAttrs['validation']) && is_array($fieldAttrs['validation'])) {
                    foreach ($fieldAttrs['validation'] as $rule => $ruleName) {
                        $params = [];
                        if (is_string($rule)) {
                            $params = (array) $ruleName;
                            $ruleName = $rule;
                        }
                        $params = array_merge(array($ruleName, $field), $params);
                        call_user_func_array(array($v, 'rule'), $params);
                    }
                }
            }
        }

        // Unique validation
        if (!empty($uniqueWhere)) {
            foreach ($uniqueWhere as $field => $value) {
                if (!is_array($value)) {
                    $value = [$field => $entity->$field];
                }
                //SMA Removed !in_array(null, $value, true) && 
                if ($this->first($value) !== false) {
                    $entity->error($field, "" . ucwords(str_replace('_', ' ', $field)) . " '" . implode('-', $value) . "' is already taken.");
                }
            }
        }

        if (!$v->validate()) {
            $entity->errors($v->errors(), false);
        }

        //SMA Added relations validation
        //TODO Review for all relations and check for it to be optional or not
        $relations = $entity->relations($this, $entity);
        if (count($relations)) {

            foreach ($relations as $relation_name => $relation) {
                $errors = [];
                $getter = 'get' . ucfirst($relation_name);
                if ($relation instanceof HasMany && is_callable([$entity, $getter])) {
                    $children = $entity->$getter();
                    if (is_array($children)) {
                        foreach ($entity->$getter() as $key => $child) {
                            if (!$this->getMapper(get_class($child))->validate($child)) {
                                $errors_child = $child->errors();
                                //Disable validation on foreign key field it will be filled up later on
                                unset($errors_child[$relation->foreignKey()]);

                                if (count($errors_child)) {
                                    $errors[$key] = $errors_child;
                                }
                            }
                        }
                    }
                }
                //TODO Check if other relations need to validated
                if (count($errors)) {
                    $entity->errors([$relation_name => $errors]);
                }
            }
        }

        // Run afterValidate to run additional/custom validations
        if (false === $this->eventEmitter()->emit('afterValidate', [$entity, $this, $v])) {
            return false;
        }

        // Return error result
        return !$entity->hasErrors();
    }

}
