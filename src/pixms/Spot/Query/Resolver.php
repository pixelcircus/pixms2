<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Spot\Query;

/**
 * Description of Resolver
 *
 * @author Sonia
 */
class Resolver extends \Spot\Query\Resolver
{
    /**
     * Migrate create schema
     *
     * @return \Doctrine\DBAL\Schema\Schema
     */
    public function migrateCreateSchema()
    {
        $entityName = $this->mapper->entity();
        $tableName = $entityName::table();
        $fields = $this->mapper->entityManager()->fields();
        $fieldIndexes = $this->mapper->entityManager()->fieldKeys();
        $schema = new \Doctrine\DBAL\Schema\Schema();
        $table = $schema->createTable($tableName);
        $table->addOption('collate', 'utf8mb4_unicode_ci');
        foreach ($fields as $field => $fieldInfo) {
            $fieldType = $fieldInfo['type'];
            unset($fieldInfo['type']);
            $table->addColumn($field, $fieldType, $fieldInfo);
        }
        // PRIMARY
        if ($fieldIndexes['primary']) {
            $table->setPrimaryKey($fieldIndexes['primary']);
        }
        // UNIQUE
        foreach ($fieldIndexes['unique'] as $keyName => $keyFields) {
            $table->addUniqueIndex($keyFields, $keyName);
        }
        // INDEX
        foreach ($fieldIndexes['index'] as $keyName => $keyFields) {
            $table->addIndex($keyFields, $keyName);
        }
        
        //TODO Should move that to the entityManager
        $relations = call_user_func(array($entityName, 'relations'), $this->mapper, new $entityName);
        foreach ($relations as $relation_name => $relation) {
            if ($relation instanceof \Spot\Relation\BelongsTo) {
                $parent_entity = $relation->entityName();
                
                $foreignTable = call_user_func(array($parent_entity, 'table'));
                
                //TODO Should be customisable, sometimes you want to delete other times you don't want to
                $table->addForeignKeyConstraint($foreignTable, array($relation->localKey()), array($relation->foreignKey()), array("onDelete" => "CASCADE"), $tableName.'_fk_'.$relation_name);
                
            }
            //TODO Other types of relations
        }
        
        return $schema;
    }
}
