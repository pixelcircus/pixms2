<?php

use Doctrine\DBAL\Types\Type;
use Spot\Config;
use Symfony\Component\HttpFoundation\Request;

return [
    'di' => [
        'env_dir' => __DIR__,
        'locales.current' => 'fr',
        'locales.fallbacks' => ['fr'],
        'Uploads.config' => [
            'formats' => [
                'slide' => [1200, 400],
                'list_thumb' => [400, 400],
                'details' => [680, 400],
            ]
        ],
        'SymfonyTranslation.config' => [
            'message_selector' => 'Symfony\Component\Translation\MessageSelector',
            'loaders' => ['array' => 'Symfony\Component\Translation\Loader\ArrayLoader'],
            'resources' => [
                'en' => [
                    ['array', require __DIR__.'/../../resources/languages/en/authSquared.php'],
                    ['array', require __DIR__.'/../../resources/languages/en/general.php'],
                    ['array', require __DIR__.'/../../resources/languages/en/datatables.php', 'datatables'],
                ],
                'fr' => [
                    ['array', require __DIR__.'/../../resources/languages/fr/authSquared.php'],
                    ['array', require __DIR__.'/../../resources/languages/fr/general.php'],
                    ['array', require __DIR__.'/../../resources/languages/fr/datatables.php', 'datatables'],
                ]
            ],
        ],
        'AuthSquared.config' => [
            'login_path' => '/login',
            'logout_path' => '/logout',
            'logged_out_message' => 'Vous avez été déconnecté.',
            'logged_in_message' => 'Vous êtes maintenant connecté.',
            'wrong_credentials_message' => 'Mauvaise combinaison courriel/mot de passe.',
            'unauthorized_message' => 'Vous devez être connecté pour accéder à cette page.',
            'logged_in_path' => '/users',
            'username_param' => 'email',
            'password_param' => 'password',
            'login_get_route' => 'Pixms\\Core\\Controllers\\LoginController::handle',
            'login_post_route' => 'Pixms\\Core\\Controllers\\LoginController::submit',
        ],
        'Mailer.config' => [
            'server' => 'Laasti\Mailer\Servers\SMTP',
            'server_args' => ['Psr\Log\LoggerInterface', getenv('SMTP_HOST')]
        ],
        'CrudModules.config' => [
            'file_server_url' => getenv('FILESERVER_URL'),
            'modules' => [
                'Pixms\Modules\Users\UsersModule',
                'Pixms\Modules\Slides\SlidesModule',
                'Pixms\Modules\News\NewsModule',
                'Pixms\Modules\Uploads\UploadsModule',
                'Pixms\Modules\News\NewsSectionsModule',
            ],
        ],
        'GregwarImage.config' => [
            'cache_dir' => getenv('CACHE_DIR').'/image',
        ],
        'Twig.config' => [
          'cache' => getenv('CACHE_DIR').'/twig',
          'templates_locations' => [
              __DIR__.'/../../resources/themes/pixms/views',
              __DIR__.'/../../resources/themes/base/views'
          ]  
        ],
        'Pixms\Modules\Users\UsersModule' => [
            'class' => 'Pixms\Modules\Users\UsersModule'
        ],
        'Pixms\Modules\Slides\SlidesModule' => [
            'class' => 'Pixms\Modules\Slides\SlidesModule'
        ],
        'Pixms\Modules\News\NewsModule' => [
            'class' => 'Pixms\Modules\News\NewsModule'
        ],
        'Pixms\Modules\Uploads\UploadsModule' => [
            'class' => 'Pixms\Modules\Uploads\UploadsModule'
        ],
        'Pixms\Modules\News\NewsSectionsModule' => [
            'class' => 'Pixms\Modules\News\NewsSectionsModule'
        ],
        'Symfony\Component\HttpFoundation\Session\SessionInterface' => [
            'class' => 'Symfony\Component\HttpFoundation\Session\Session',
            'singleton' => true,
        ],
        'Pixms\AuthSquared\Middlewares\AuthorizationMiddleware' => [
            'class' => 'Pixms\AuthSquared\Middlewares\AuthorizationMiddleware',
            'arguments' => [
                'Pixms\AuthSquared\AuthSquared', '_controller', 'Laasti\Notifications\NotificationService', 'Laasti\Response\ResponderInterface'
            ]
        ],
        'Pixms\AuthSquared\Interfaces\PasswordHandlerInterface' => [
            'class' => 'Pixms\AuthSquared\PasswordHandler'
        ],
        'Pixms\AuthSquared\Interfaces\UserRepositoryInterface' => [
            'class' => 'Pixms\Modules\Users\UserMapper',
            'arguments' => ['Spot\Locator']
        ],
        'Pixms\Files\FileValidator' => [
            'class' => 'Pixms\Files\FileValidator',
            'arguments' => ['Symfony\Component\HttpFoundation\Request', 'Symfony\Component\HttpFoundation\Session\SessionInterface']
        ],
        'FlySystem.config' => [
            'uploads' => ['League\Flysystem\Adapter\Local', [getenv('UPLOADS_DIR')]],
            //Make a temp adapter the auto garbage collect after a certain time
            'temp' => ['League\Flysystem\Adapter\Local', [getenv('TEMP_DIR')]],
            
        ],
        'Pixms\Modules\Users\UserMapper' => [
            'class' => 'Pixms\Modules\Users\UserMapper',
            'arguments' => ['Spot\Locator'],
            'singleton' => true,
        ],
        'Pixms\\Core\\Controllers\\HomeController' => [
            'class' => 'Pixms\\Core\\Controllers\\HomeController',
            'arguments' => ['Laasti\Response\ResponderInterface']
        ],
        'Pixms\\Core\\Controllers\\LoginController' => [
            'class' => 'Pixms\\Core\\Controllers\\LoginController',
            'arguments' => ['Laasti\Response\ResponderInterface', 'Symfony\Component\Translation\Translator']
        ],
        'Pixms\\Core\\Controllers\\ForgotPasswordController' => [
            'class' => 'Pixms\\Core\\Controllers\\ForgotPasswordController',
            'arguments' => [
                'Laasti\Response\ResponderInterface', 'Symfony\Component\Translation\Translator', 
                'Pixms\Modules\Users\UserMapper',
                'Laasti\Notifications\NotificationService', 'Laasti\Mailer\Mailer'
            ]
        ],
        'ImageServerMapper' => [
            'class' => 'Pixms\Spot\PixmsMapper',
            'arguments' => ['Spot\Locator', 'Pixms\Modules\Uploads\Entities\UploadEntity']
        ],
        'Pixms\\Files\\ImageServer' => [
            'class' => 'Pixms\\Files\\ImageServer',
            'arguments' => [
                'Laasti\Response\ResponderInterface', 'Gregwar\Image\Image',
                'League\Flysystem\MountManager','ImageServerMapper', 'Uploads.config'
            ]
        ],
        'Pixms\Url\UrlFactory'=>[
            'class' => 'Pixms\Url\UrlFactory',
            'arguments' => ['Symfony\Component\HttpFoundation\Request']
        ],
        'Pixms\Core\Middlewares\SessionMiddleware'=>[
            'class' => 'Pixms\Core\Middlewares\SessionMiddleware',
            'arguments' => ['Symfony\Component\HttpFoundation\Session\SessionInterface']
        ],
        'Pixms\Views\PrepViewMiddleware' => [
            'class' => 'Pixms\Views\PrepViewMiddleware',
            'arguments' => [
                'Laasti\Response\ResponderInterface', 
                'Symfony\Component\HttpFoundation\Session\SessionInterface', 
                'Pixms\AuthSquared\AuthSquared',
                'viewdata',
            ]        
        ],
        'Laasti\Notifications\NotificationMiddleware' => [
            'class' => 'Laasti\Notifications\NotificationMiddleware',
            'arguments' => ['Laasti\Notifications\NotificationService']        
        ],
        'Laasti\Response\Engines\TemplateEngineInterface' => [
            'class' => 'Pixms\Views\TwigRenderer',
            'arguments' => ['Twig_Environment'],
            'singleton' => true
        ],
        'Pixms\Views\TwigRenderer' => [
            'class' => 'Pixms\Views\TwigRenderer',
            'arguments' => ['Twig_Environment'],
            'singleton' => true
        ],
        'Symfony\Component\HttpFoundation\Request' => [
            'definition' => function() {
                return Request::createFromGlobals();
            },
            'singleton' => true,
        ],
        'Laasti\Notifications\NotificationService' => [
            'class' => 'Laasti\Notifications\NotificationService',
            'arguments' => ['Symfony\Component\HttpFoundation\Session\SessionInterface'],
            'singleton' => true,
        ],
        'Symfony\Component\HttpFoundation\RedirectResponse' => [
            'class' => 'Pixms\Url\RedirectResponse',
            'arguments' => ['Pixms\Url\UrlFactory']
        ],
        'Spot\Config' => [
            'definition' => function() {
                $cfg = new Config();
                //TODO Better integration
                Type::addType('file', 'Pixms\Spot\Types\FileType');
                Type::addType('files', 'Pixms\Spot\Types\FilesType');
                $conn = $cfg->addConnection(getenv('DATABASE_DRIVER'), getenv('DATABASE_DSN'));
                $conn->getDatabasePlatform()->registerDoctrineTypeMapping('varchar', 'file');
                $conn->getDatabasePlatform()->registerDoctrineTypeMapping('varchar', 'files');
                return $cfg;
            },
            'singleton' => true,
        ],
                    'viewdata' => [
        'menu' => [
            "classes" => ["main-menu"],
            'items' => [
                [
                    'name' => 'menu_users',
                    'url' => '/users',
                ],
                [
                    'name' => 'menu_pages',
                    'url' => '/pages',
                ],
                [
                    'name' => 'menu_news',
                    'url' => '/news',
                ],
                [
                    'name' => 'menu_slides',
                    'url' => '/slides',
                ],
            ]
        ],
        /*                'top_menu' => [
            "classes" => ["top-menu"],
            'items' => [
                [
                    'name' => '<span class="icon-chart-pie"></span> Dashboard',
                    'url' => '#',
                ],
                [
                    'name' => 'Dashboard',
                    'url' => '#',
                ],
            ]
        ],
                        'user_menu'=> [
            "classes" => ["user-sub-menu"],
            'items' => [
                [
                    'name' => '<span class="icon-chart-pie"></span> Dashboard',
                    'url' => '#',
                ],
                [
                    'name' => 'Dashboard',
                    'url' => '#',
                ],
            ]
        ],*/
        'site_name' => 'Pixms',
        'locale' => 'en',
        'logout_url' => '/logout',
        'assets_path' => '/../../resources/themes/pixms/public/assets/'

    ],
    ],
    'routes' => [
        ['GET', '/', 'Pixms\\Core\\Controllers\\HomeController::handle'],
        ['GET', '/forgot-password/{token:hex}', 'Pixms\\Core\\Controllers\\ForgotPasswordController::reset_form'],
        ['POST', '/forgot-password/{token:hex}', 'Pixms\\Core\\Controllers\\ForgotPasswordController::reset_password'],
        ['GET', '/forgot-password', 'Pixms\\Core\\Controllers\\ForgotPasswordController::handle'],
        ['POST', '/forgot-password', 'Pixms\\Core\\Controllers\\ForgotPasswordController::submit'],
        ['GET', '/i/crop/{width:number}/{height:number}/{path:path}', 'Pixms\\Files\\ImageServer::crop'],
        ['GET', '/i/maxwidth/{width:number}/{path:path}', 'Pixms\\Files\\ImageServer::maxwidth'],
        ['GET', '/i/maxheight/{height:number}/{path:path}', 'Pixms\\Files\\ImageServer::maxheight'],
        ['GET', '/i/fit/{width:number}/{height:number}/{path:path}', 'Pixms\\Files\\ImageServer::fit'],
        ['GET', '/i/{format:word}/{path:path}', 'Pixms\\Files\\ImageServer::format'],
    ],
    'providers' => [
        'Pixms\Core\Providers\TwigServiceProvider',
        'Laasti\Providers\SpotProvider',
        'Pixms\AuthSquared\Providers\LeagueProvider',
        'Pixms\CrudModules\CrudModulesProvider',
    ],
    'middlewares' => [
        'Laasti\Route\Middlewares\RouteMiddleware',
        'Pixms\Core\Middlewares\SessionMiddleware',
        'Laasti\Notifications\NotificationMiddleware',
        'Pixms\AuthSquared\Middlewares\AuthenticationMiddleware',//
        'Pixms\Views\PrepViewMiddleware', //
        'Pixms\CrudModules\Middlewares\ModulesMiddleware',
        'Pixms\AuthSquared\Middlewares\AuthorizationMiddleware',
        'Laasti\Route\Middlewares\TwoStepControllerMiddleware'
    ],
    
];
