<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Views;

use Laasti\Response\Engines\TemplateEngineInterface;
use Twig_Environment;

/**
 * Description of TwigRenderer
 *
 * @author Sonia
 */
class TwigRenderer implements TemplateEngineInterface
{
    protected $twig;
    
    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }
    
    public function render($name, array $context = array()) {
        return $this->twig->render($name, $context);
    }
}
