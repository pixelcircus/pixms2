<?php

namespace Pixms\Views;

use Laasti\Response\ResponderInterface;
use Laasti\Stack\MiddlewareInterface;
use Pixms\AuthSquared\AuthSquared;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrepViewMiddleware
 *
 * @author Sonia
 */
class PrepViewMiddleware implements MiddlewareInterface
{

    /**
     *
     * @var SessionInterface 
     */
    protected $session;
    protected $responder;
    protected $authentication;
    protected $viewdata;

    public function __construct(ResponderInterface $responder, SessionInterface $session, AuthSquared $authentication, $viewdata = [])
    {
        $this->session = $session;
        $this->responder = $responder;
        $this->authentication = $authentication;
        $this->viewdata = $viewdata;
    }

    public function handle(Request $request)
    {
        $this->responder->addData($this->viewdata);
        if ($this->authentication->isAuthenticated()) {
            $this->responder->setData('user', $this->authentication->getAuthenticatedUser());
        }
        $this->responder->setData('base_path', $request->getBaseUrl());
        $this->responder->setData('logout_url', $request->getBaseUrl().$this->viewdata['logout_url']);
        $menu = $this->responder->getData('menu');
        
        foreach ($menu['items'] as $key => $item) {
            $menu['items'][$key]['url'] = $request->getBaseUrl().$item['url'];
        }
        $this->responder->setData('menu', $menu);
        
        
        $this->responder->setData('notifications', $this->session->getFlashBag()->all());
        
        return $request;
    }

}
