<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Views;

use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Laasti\Response\Engines\TemplateEngineInterface;

/**
 * Description of ViewResponse
 *
 * @author Sonia
 */
class ViewResponse extends Response
{
    protected $template;
    protected $view;
    protected $context;
    
    public function __construct(TemplateEngineInterface $template = null, $view = null, $context = [], $status = 200, $headers = array())
    {
        $this->template = $template;
        $this->view = $view;
        $this->context = $context;
        
        parent::__construct('', $status, $headers);
    }
    
    public function setView($view) {
        $this->view = $view;
        return $this;
    }
    
    public function setContext($view) {
        $this->context = $view;
        return $this;
    }
    
    public function setData($key, $value) {
        $this->context[$key] = $value;
        return $this;
    }
    
    public function getContent()
    {
    }
    
    
    public function sendContent()
    {
        if (is_null($this->view)) {
            throw new RuntimeException('No view was set for the ViewResponse object.');
        }
        echo $this->template->render($this->view, $this->context);

        return $this;
    }

}
