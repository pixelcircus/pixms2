<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Views;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of DataTableController
 *
 * @author Sonia
 */
class DisplayController
{
    
    protected $response;
    protected $repository;
    protected $responder;
    
    public function __construct($command, ViewResponse $response, \Laasti\Response\ResponderInterface $responder)
    {
        $this->command = $command;
        $this->response = $response;
        $this->responder = $responder;
    }

    public function respond(Request $request)
    {
        $answer = $this->command->run();
        $this->response->setContext(array_merge($this->responder->exportData(), $answer['payload']));
        return $this->response;
    }
}
