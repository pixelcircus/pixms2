<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News;

use League\Container\ContainerInterface;
use Pixms\CrudModules\AbstractModule;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Pixms\Spot\PixmsMapper;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class NewsSectionsModule extends AbstractModule implements ModuleInterface
{
    protected $baseDir = __DIR__;
    protected $identifier = 'newssections';
    
    protected function loadLanguageFiles()
    {
        $c = $this->getContainer();
        $t = $c->get('Symfony\Component\Translation\Translator');
        if (is_dir($this->baseDir.'/languages')) {
            $files = array_diff(scandir($this->baseDir.'/languages/newssections'), ['.', '..']);
            foreach ($files as $file) {
                $lang = str_replace('.php', '', $file);
                $t->addResource('array', require $this->baseDir.'/languages/newssections/'.$file, $lang, $this->getIdentifier());
            }
        }
    }
    
    public function getConfig()
    {
        $c = $this->getContainer();
        return [
            $this->getFormFactory() => [
                'class' => 'Pixms\Modules\News\Forms\NewsSectionFormFactory',
                'arguments' => ['Pixms\Url\UrlFactory', 'Symfony\Component\Translation\Translator']
            ],
            $this->getEntity() => [
                'class' => 'Pixms\Modules\News\Entities\NewsSectionEntity'
            ],
            $this->getMapper() => [
                'definition' => function() use ($c) {
                    return new PixmsMapper($c->get('Spot\Locator'), 'Pixms\Modules\News\Entities\NewsSectionEntity');
                },
                'singleton' => true
            ],
            $this->getDataTableFactory() => [
                'class' => 'Pixms\Modules\News\Tables\NewsSectionsDataTableFactory',
                'arguments' => ['Pixms\Url\UrlFactory', 'Symfony\Component\Translation\Translator']
            ],
            $this->getCrudService() => [
                'class' => 'Pixms\CrudModules\CrudService',
                'arguments' => [$this->getMapper(), 'CrudModulesDataTableDecorator', 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator']
            ],
        ];
    }
    
    public function getRoutes() {
        return [
            ['GET', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/create', 'Pixms\CrudModules\Controllers\CreateController::display'],
            ['POST', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/create', 'Pixms\CrudModules\Controllers\CreateController::submit'],
            ['GET', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/details/{id:number}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
            ['GET', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/delete/{id:number}', 'Pixms\CrudModules\Controllers\DeleteController::handle'],
            ['GET', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/activate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::activate'],
            ['GET', '/{parent_module:(?:news)}/{parent_id:number}/{module:(?:'.$this->getIdentifier().')}/deactivate/{id:number}', 'Pixms\CrudModules\Controllers\StatusController::deactivate'],
            ['POST', '/{module:(?:'.$this->getIdentifier().')}/reorder', 'Pixms\CrudModules\Controllers\ReorderController::handle'],
        ];
    }
    
}
