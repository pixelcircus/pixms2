<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Entities;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class NewsEntity extends Entity
{

    protected static $table = 'news';
    protected static $mapper = 'Pixms\Spot\PixmsMapper';

    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'status' => ['type' => 'smallint', 'required' => true, 'default' => 1, 'choices' => [1,0]],
            'date_publish' => ['type' => 'date', 'required' => true, 'default' => strftime('%Y-%m-%d'), 'validation' => ['date']],
            'image' => ['type' => 'file', 'required' => true, 'validation' => ['image', 'minwidth' => 800, 'minheight' => 400]],
            'title_fr' => ['type' => 'string', 'required' => true],
            'title_en' => ['type' => 'string', 'required' => true],
            'category_fr' => ['type' => 'string'],
            'category_en' => ['type' => 'string'],
            'description_fr' => ['type' => 'text'],
            'description_en' => ['type' => 'text'],
            'parent_id' => ['type' => 'integer', 'index' => true]
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'children' => $mapper->hasMany($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'parent_id'),
            'parent' => $mapper->belongsTo($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'parent_id'),
            'sections' => $mapper->hasMany($entity, 'Pixms\Modules\News\Entities\NewsSectionEntity', 'parent_id'),
            'related_news' => $mapper->hasManyThrough($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'Pixms\Modules\News\Entities\NewsRelatedEntity', 'related_id', 'news_id')
        ];
    }
}
