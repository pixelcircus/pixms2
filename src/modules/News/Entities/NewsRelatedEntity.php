<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Entities;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class NewsRelatedEntity extends Entity
{

    protected static $table = 'news_related';
    protected static $mapper = 'Pixms\Spot\PixmsMapper';

    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'news_id' => ['type' => 'integer', 'index' => true, 'required' => true],
            'related_id' => ['type' => 'integer', 'index' => true, 'required' => true]
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'news' => $mapper->hasOne($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'news_id'),
            'related' => $mapper->hasOne($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'related_id')
        ];
    }
}
