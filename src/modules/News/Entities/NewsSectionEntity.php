<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Entities;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class NewsSectionEntity extends Entity
{

    use \Pixms\CrudModules\Components\Reorder\OrderableEntityTrait;
    protected static $orderableField = 'priority';
    protected static $table = 'news_sections';
    protected static $baseUri = '/news/{parent_id}/newssections';
    protected static $mapper = 'Pixms\Spot\PixmsMapper';

    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'priority' => ['type' => 'integer', 'required' => true],
            'status' => ['type' => 'smallint', 'required' => true, 'default' => 1, 'choices' => [1, 0]],
            'image' => ['type' => 'file', 'validation' => ['image', 'minwidth' => 800, 'minheight' => 400]],
            'title_fr' => ['type' => 'string', 'required' => true],
            'title_en' => ['type' => 'string', 'required' => true],
            'description_fr' => ['type' => 'text'],
            'description_en' => ['type' => 'text'],
            'parent_id' => ['type' => 'integer', 'index' => true, 'required' => true]
        ];
    }

    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'parent' => $mapper->belongsTo($entity, 'Pixms\Modules\News\Entities\NewsEntity', 'parent_id')
        ];
    }

    public static function baseUri() {
        return self::$baseUri;
    }
}
