<?php

namespace Pixms\Modules\News\Tables;

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\Translation\Translator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataTableBuilder
 *
 * @author Sonia
 */
class NewsSectionsDataTableFactory implements DataTableFactoryInterface
{

    protected $url;
    protected $trans;

    public function __construct(UrlFactory $url, Translator $trans)
    {
        $this->url = $url;
        $this->trans = $trans;
    }

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns(['priority' => $this->trans->trans('field_priority'),'title_fr' => $this->trans->trans('field_title'), 'image' => $this->trans->trans('field_image'), 'status' => $this->trans->trans('field_status')])
                ->addClasses('priority', ['icon-column'] )
                ->setAttributes(['data-reorder' => 'priority', 'data-reorder-url' => $this->url->create('/newssections/reorder')])
                ->addAction(new DataTableAction('edit', $this->trans->trans('action_edit'), array($this, 'editUrl')))
                ->addAction(new DataTableAction('cancel-circle', $this->trans->trans('action_delete'), array($this, 'deleteUrl')));

        return $table;
    }

    public function editUrl($row)
    {
        return $this->url->create('/news/{id}/newssections/details/' . $row->id);
    }

    public function deleteUrl($row)
    {
        return $this->url->create('/news/{id}/newssections/delete/' . $row->id);
    }

}
