<?php

return [
    'title_list' => 'News',
    'title_create' => 'New news',
    'title_update' => 'Update news',
    'title_sections' => 'Sections',
    'button_create' => 'Add a news',
    'button_create_section' => 'Add a section',
    'callback_created' => 'The news has been created.',
    'callback_updated' => 'The news has been updated.',
    'callback_activated' => 'The news has been activated.',
    'callback_already_activated' => 'The news is already active.',
    'callback_deactivated' => 'The news has been deactivated.',
    'callback_already_deactivated' => 'The news is already inactive.',
    'callback_deleted' => 'The news has been deleted.',
    'callback_not_exists' => 'The news does not exist.',
    'choices_status_0' => 'Inactive',
    'choices_status_1' => 'Active',
    'field_related_news' => 'Related news',
];