<?php

return [
    'title_list' => 'Nouvelles',
    'title_create' => 'Nouvelle nouvelle',
    'title_update' => 'Mise à jour de la nouvelle',
    'title_sections' => 'Sections',
    'button_create' => 'Ajouter une nouvelle',
    'button_create_section' => 'Ajouter une section',
    'callback_created' => 'La nouvelle a été créée.',
    'callback_updated' => 'La nouvelle a été mise à jour.',
    'callback_activated' => 'La nouvelle a été activée.',
    'callback_already_activated' => 'La nouvelle est déjà active.',
    'callback_deactivated' => 'La nouvelle a été désactivée.',
    'callback_already_deactivated' => 'La nouvelle est déjà inactive.',
    'callback_deleted' => 'La nouvelle a été supprimée.',
    'callback_not_exists' => 'La nouvelle n\existe pas.',
    'choices_status_0' => 'Inactif',
    'choices_status_1' => 'Actif',
    'field_related_news' => 'Nouvelle(s) liée(s)',
];