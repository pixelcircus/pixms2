<?php

return [
    'title_list' => 'Sections',
    'title_create' => 'New section',
    'title_update' => 'Update section',
    'title_sections' => 'Sections',
    'button_create' => 'Add a section',
    'callback_created' => 'The section has been created.',
    'callback_updated' => 'The section has been updated.',
    'callback_activated' => 'The section has been activated.',
    'callback_already_activated' => 'The section is already active.',
    'callback_deactivated' => 'The section has been deactivated.',
    'callback_already_deactivated' => 'The section is already inactive.',
    'callback_deleted' => 'The section has been deleted.',
    'callback_not_exists' => 'The section does not exist.',
    'choices_status_0' => 'Inactive',
    'choices_status_1' => 'Active',
];