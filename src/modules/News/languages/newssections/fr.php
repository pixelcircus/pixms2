<?php

return [
    'title_list' => 'Sections',
    'title_create' => 'Nouvelle section',
    'title_update' => 'Mise à jour de la section',
    'button_create' => 'Ajouter une section',
    'callback_created' => 'La section a été créée.',
    'callback_updated' => 'La section a été mise à jour.',
    'callback_activated' => 'La section a été activée.',
    'callback_already_activated' => 'La section est déjà active.',
    'callback_deactivated' => 'La section a été désactivée.',
    'callback_already_deactivated' => 'La section est déjà inactive.',
    'callback_deleted' => 'La section a été supprimée.',
    'callback_not_exists' => 'La section n\existe pas.',
    'choices_status_0' => 'Inactif',
    'choices_status_1' => 'Actif',
];