<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Forms;

use Pixms\CrudModules\CrudService;
use Pixms\CrudModules\Decorators\CollectionDecorator;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Modules\News\Tables\NewsSectionsDataTableFactory;
use Pixms\Url\UrlFactory;
use Symfony\Component\Translation\Translator;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class NewsUpdateFormFactory extends NewsFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $decorator;
    protected $service;
    protected $trans;

    public function __construct(UrlFactory $url, CrudService $service, CollectionDecorator $decorator, Translator $trans)
    {
        parent::__construct($url, $service, $trans);
        $this->url = $url;
        $this->decorator = $decorator;
        $this->service = $service;
        $this->trans = $trans;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = $this->getMainForm($action, $data, $errors);
        $factory = new NewsSectionsDataTableFactory($this->url, $this->trans);
        $viewdata = ['datatable' => $factory->create($this->decorator->create($this->service->getRelatedCollection('sections', $data)))];
        $form->addSection('', ['datatable', 'full', 'line-before'])
                //TODO Dehardcode URL
                ->addItem('title', $this->trans->trans('title_sections', [], 'news'))
                ->addItem('link', $this->trans->trans('button_create_section', [], 'news'), null, ['url' => $this->url->create('/news/{id}/newssections/create'), 'input_attributes' => ['class' => 'button']])
                ->addItem('view', '', null, ['view' => 'includes/datatable.twig', 'viewdata' => $viewdata])
                ->end();
        $this->addButtons($form);
        
        return $form;
    } 
}
