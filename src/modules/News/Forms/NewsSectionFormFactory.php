<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Forms;

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\Translation\Translator;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class NewsSectionFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $trans;
    
    protected $entityName = "Pixms\Modules\News\Entities\NewsSectionEntity";

    public function __construct(UrlFactory $url, Translator $trans)
    {
        $this->url = $url;
        $this->trans = $trans;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $form->addSection('', ['two-columns'])
                ->addItem('radio', $this->trans->trans('field_status'), 'status', ['choices' => $this->getChoices('status'), 'default' => 1])->end()
                ->addSection('', ['two-columns'])->end();
        $this->addLocaleForm($form, 'fr');
        $this->addLocaleForm($form, 'en');
        $form->addSection('', ['two-columns'])->addItem('file', $this->trans->trans('field_image'), 'image')->end();
        $form->addSection('', ['buttons'])
                ->addItem('link', $this->trans->trans('button_cancel'), null, ['url' => $this->url->create('/news/details/{parent_id}'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', $this->trans->trans('button_submit'), null, [ 'input_attributes' => ['class' => 'ok']])->end();
        
        return $form;
    } 
    
    public function addLocaleForm($form, $locale) {
        $form->addSection('', ['two-columns'])
                ->addItem('text', $this->trans->trans('field_title').' ('.$locale.')', 'title_'.$locale)
                ->addItem('textarea', $this->trans->trans('field_description').' ('.$locale.')', 'description_'.$locale, ['attributes' => ['data-editor' => true]])
                ->end();
    }
    
    public function getChoices($field) {
        $fields = call_user_func(array($this->entityName, 'fields'));
        $choices = [];
        foreach ($fields[$field]['choices'] as $choice) {
            $choices[$choice] = $this->trans->trans('choices_'.$field.'_'.$choice, [], 'newssections');
        }
        return $choices;
    }
}
