<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News\Forms;

use Pixms\CrudModules\CrudService;
use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\Translation\Translator;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class NewsFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $service;
    protected $trans;
    
    protected $entityName = "Pixms\Modules\News\Entities\NewsEntity";

    public function __construct(UrlFactory $url, CrudService $service, Translator $trans)
    {
        $this->url = $url;
        $this->service = $service;
        $this->trans = $trans;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = $this->getMainForm($action, $data, $errors);
        $this->addButtons($form);
        return $form;
    }

    public function getMainForm($action, array $data, array $errors = [])
    {

        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $edit_url = !empty($data['id']) ? $this->url->create('/uploads/details/news/{id}/image/') : null;
        $back_uri = !empty($data['id']) ? $this->url->parseUri('/news/details/{id}') : null;
        $form->addSection('', ['two-columns'])
                ->addItem('radio', $this->trans->trans('field_status'), 'status', ['choices' => $this->getChoices('status'), 'default' => 1])->end()
                ->addSection('', ['two-columns'])->addItem('date', $this->trans->trans('field_date_publish'), 'date_publish', ['default' => strftime('%Y-%m-%d')])->end();
        $this->addLocaleForm($form, 'fr');
        $this->addLocaleForm($form, 'en');
        $form->addSection('', ['two-columns'])
                ->addItem('file', $this->trans->trans('field_image'), 'image', ['edit_url' => $edit_url, 'back_url' => $back_uri])
                ->addItem('select', $this->trans->trans('field_parent_id'), 'parent_id', ['choices' => $this->service->getRelationChoices('parent', 'title_fr', [], ['title_fr' => 'ASC'])])
                ->end();
        $form->addSection('', ['two-columns'])
                ->addItem('select', $this->trans->trans('field_related_news', [], 'news'), 'related_news[]', ['id' => 'related_news', 'choices' => $this->service->getRelationChoices('related_news', 'title_fr', [], ['title_fr' => 'ASC']), 'input_attributes' => ['multiple' => true]])->end();
        return $form;
    }

    public function addButtons($form)
    {
        $form->addSection('', ['buttons'])
                ->addItem('link', $this->trans->trans('button_cancel'), null, ['url' => $this->url->create('/news'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', $this->trans->trans('button_submit'), null, [ 'input_attributes' => ['class' => 'ok']])->end();
        return $this;
    }
    
    public function addLocaleForm($form, $locale) {
        $form->addSection('', ['two-columns', 'line-before'])
                ->addItem('text', $this->trans->trans('field_title').' ('.$locale.')', 'title_'.$locale)
                ->addItem('text', $this->trans->trans('field_category').' ('.$locale.')', 'category_'.$locale)
                ->addItem('textarea', $this->trans->trans('field_description').' ('.$locale.')', 'description_'.$locale, ['attributes' => ['data-editor' => true]])
                ->end();
    }
    
    public function getChoices($field) {
        $fields = call_user_func(array($this->entityName, 'fields'));
        $choices = [];
        foreach ($fields[$field]['choices'] as $choice) {
            $choices[$choice] = $this->trans->trans('choices_'.$field.'_'.$choice, [], 'news');
        }
        return $choices;
    }
    
}
