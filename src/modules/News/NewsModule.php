<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\News;

use Pixms\CrudModules\AbstractModule;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Pixms\Spot\PixmsMapper;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class NewsModule extends AbstractModule implements ModuleInterface
{
    protected $baseDir = __DIR__;
    protected $identifier = 'news';
    
    public function getConfig() 
    {
        $c = $this->getContainer();
        return [
            $this->getFormFactory() => [
                'definition' => function() use ($c) {
                    $request = $c->get('Symfony\Component\HttpFoundation\Request');
                    if ($request->attributes->get('id')) {
                        return new \Pixms\Modules\News\Forms\NewsUpdateFormFactory($c->get('Pixms\Url\UrlFactory'), $c->get($this->getCrudService()), $c->get('CrudModulesDataTableDecorator'), $c->get('Symfony\Component\Translation\Translator'));
                    } else {
                        return new \Pixms\Modules\News\Forms\NewsFormFactory($c->get('Pixms\Url\UrlFactory'), $c->get($this->getCrudService()), $c->get('Symfony\Component\Translation\Translator'));
                    }
                }
            ],
            $this->getEntity() => [
                'class' => 'Pixms\Modules\News\Entities\NewsEntity'
            ],
            $this->getMapper() => [
                'definition' => function() use ($c) {
                    return new PixmsMapper($c->get('Spot\Locator'), 'Pixms\Modules\News\Entities\NewsEntity');
                },
                'singleton' => true
            ],
            $this->getDataTableFactory() => [
                'class' => 'Pixms\Modules\News\Tables\NewsDataTableFactory',
                'arguments' => ['Pixms\Url\UrlFactory', 'Symfony\Component\Translation\Translator']
            ],
            $this->getCrudService() => [
                'class' => 'Pixms\CrudModules\CrudService',
                'arguments' => [$this->getMapper(), 'CrudModulesDataTableDecorator', 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator']
            ],
        ];
    }
    
}
