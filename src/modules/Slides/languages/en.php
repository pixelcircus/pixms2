<?php

return [
    'title_list' => 'Slideshow',
    'title_create' => 'New slide',
    'title_update' => 'Update slide',
    'button_create' => 'Add a slide',
    'callback_created' => 'The slide has been created.',
    'callback_updated' => 'The slide has been updated.',
    'callback_activated' => 'The slide has been activated.',
    'callback_already_activated' => 'The slide is already active.',
    'callback_deactivated' => 'The slide has been deactivated.',
    'callback_already_deactivated' => 'The slide is already inactive.',
    'callback_deleted' => 'The slide has been deleted.',
    'callback_not_exists' => 'The slide does not exist.',
    'choices_status_0' => 'Inactive',
    'choices_status_1' => 'Active',
];