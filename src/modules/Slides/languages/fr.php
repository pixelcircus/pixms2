<?php

return [
    'title_list' => 'Diaporama',
    'title_create' => 'Nouvelle diapositive',
    'title_update' => 'Mise à jour de la dispositive',
    'button_create' => 'Ajouter une diapositive',
    'callback_created' => 'La diapositive a été créée.',
    'callback_updated' => 'La diapositive a été mise à jour.',
    'callback_activated' => 'La diapositive a été activée.',
    'callback_already_activated' => 'La diapositive est déjà active.',
    'callback_deactivated' => 'La diapositive a été désactivée.',
    'callback_already_deactivated' => 'La diapositive est déjà inactive.',
    'callback_deleted' => 'La diapositive a été supprimée.',
    'callback_not_exists' => 'La diapositive n\existe pas.',
    'choices_status_0' => 'Inactif',
    'choices_status_1' => 'Actif',
];