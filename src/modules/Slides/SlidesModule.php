<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Slides;

use Pixms\CrudModules\AbstractModule;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Pixms\Spot\PixmsMapper;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class SlidesModule extends AbstractModule implements ModuleInterface
{
    protected $baseDir = __DIR__;
    protected $identifier = 'slides';
    protected $formFactory = 'Pixms\Modules\Slides\Forms\SlideFormFactory';
    protected $entity = 'Pixms\Modules\Slides\Entities\SlideEntity';
    protected $mapper = 'Pixms\Modules\Slides\SlidesMapper';
    protected $dataTableFactory = 'Pixms\Modules\Slides\Tables\SlideDataTableFactory';
    
    //TODO: Change config format to 'formFactory' => [], 'dataTable' => [], etc. and do the wiring automatically
    //If it's only a class you can do 'formFactory' => 'Some\Class'
    public function getConfig()
    {
        $c = $this->getContainer();
        return [
            $this->getFormFactory() => [
                'class' => 'Pixms\Modules\Slides\Forms\SlideFormFactory',
                'arguments' => ['Pixms\Url\UrlFactory', 'Symfony\Component\Translation\Translator']
            ],
            $this->getEntity() => [
                'class' => 'Pixms\Modules\Slides\Entities\SlideEntity'
            ],
            $this->getMapper() => [
                'definition' => function() use ($c) {
                    return new PixmsMapper($c->get('Spot\Locator'), 'Pixms\Modules\Slides\Entities\SlideEntity');
                },
                'singleton' => true
            ],
            $this->getDataTableFactory() => [
                'class' => 'Pixms\Modules\Slides\Tables\SlideDataTableFactory',
                'arguments' => ['Pixms\Url\UrlFactory', 'Symfony\Component\Translation\Translator']
            ],
            $this->getCrudService() => [
                'class' => 'Pixms\CrudModules\CrudService',
                'arguments' => [$this->getMapper(), 'CrudModulesDataTableDecorator', 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator']
            ],
        ];
    }
}
