<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Slides\Entities;

use Spot\Entity;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class SlideEntity extends Entity
{

    use \Pixms\CrudModules\Components\Reorder\OrderableEntityTrait;
    protected static $orderableField = 'priority';
    protected static $table = 'slides';
    protected static $mapper = 'Pixms\Spot\PixmsMapper';

    public static function fields()
    {
        return [
            'priority' => ['type' => 'integer', 'required' => true],
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'status' => ['type' => 'smallint', 'required' => true, 'default' => 1, 'choices' => array(0, 1)],
            'date_publish' => ['type' => 'date', 'required' => true, 'default' => strftime('%Y-%m-%d'), 'validation' => ['date']],
            'image' => ['type' => 'file', 'required' => true, 'validation' => ['image', 'minwidth' => 800, 'minheight' => 400]],
            'title_fr' => ['type' => 'string', 'required' => true],
            'title_en' => ['type' => 'string', 'required' => true],
            'category_fr' => ['type' => 'string'],
            'category_en' => ['type' => 'string'],
            'link_fr' => ['type' => 'string', 'validation' => ['url']],
            'link_en' => ['type' => 'string', 'validation' => ['url']],
            'description_fr' => ['type' => 'text'],
            'description_en' => ['type' => 'text'],
        ];
    }
}
