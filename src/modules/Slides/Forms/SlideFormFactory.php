<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Slides\Forms;

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Url\UrlFactory;
use Symfony\Component\Translation\Translator;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class SlideFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $trans;
    protected $entityName = "Pixms\Modules\Slides\Entities\SlideEntity";

    public function __construct(UrlFactory $url, Translator $trans)
    {
        $this->url = $url;
        $this->trans = $trans;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $edit_url = !empty($data['id']) ? $this->url->create('/uploads/details/slides/{id}/image/') : null;
        $back_uri = !empty($data['id']) ? $this->url->parseUri('/slides/details/{id}') : null;
        $form->addSection('', ['two-columns'])
                ->addItem('radio', $this->trans->trans('field_status'), 'status', ['choices' => $this->getChoices('status'), 'default' => 1])->end()
                ->addSection('', ['two-columns'])->addItem('date', $this->trans->trans('field_date_publish'), 'date_publish', ['default' => strftime('%Y-%m-%d')])->end();
        $this->addLocaleForm($form, 'fr');
        $this->addLocaleForm($form, 'en');
        $form->addSection('', ['two-columns'])->addItem('file', $this->trans->trans('field_image'), 'image', ['edit_url' => $edit_url, 'back_url' => $back_uri])->end();
        $form->addSection('', ['buttons'])
                ->addItem('link', $this->trans->trans('button_cancel'), null, ['url' => $this->url->create('/slides'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', $this->trans->trans('button_submit'), null, [ 'input_attributes' => ['class' => 'ok']])->end();
        
        return $form;
    } 
    
    public function addLocaleForm($form, $locale) {
        $form->addSection('', ['two-columns'])
                ->addItem('text', $this->trans->trans('field_title').' ('.$locale.')', 'title_'.$locale)
                ->addItem('text', $this->trans->trans('field_category').' ('.$locale.')', 'category_'.$locale)
                ->addItem('text', $this->trans->trans('field_link').' ('.$locale.')', 'link_'.$locale)
                ->addItem('textarea', $this->trans->trans('field_description').' ('.$locale.')', 'description_'.$locale)
                ->end();
    }
    
    public function getChoices($field) {
        $fields = call_user_func(array($this->entityName, 'fields'));
        $choices = [];
        foreach ($fields[$field]['choices'] as $choice) {
            $choices[$choice] = $this->trans->trans('choices_'.$field.'_'.$choice, [], 'slides');
        }
        return $choices;
    }
    
}
