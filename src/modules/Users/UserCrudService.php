<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Users;

use DateTime;
use Pixms\CrudModules\CrudService;
use Pixms\Spot\Exceptions\EntityNotFoundException;
use Pixms\Spot\Exceptions\EntityNotSavedException;
use Spot\Relation\BelongsTo;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of UserCrudService
 *
 * @author Sonia
 */
class UserCrudService extends CrudService
{
    public function updateFromRequest(Request $request) {
        $pk = $this->repository->build([])->primaryKeyField();
        $entity = $this->repository->get(urldecode($request->attributes->get($pk)));
        if ($entity === false) {
            throw new EntityNotFoundException();
        }
        $this->requestEntity = $entity;
        
        if ($request->getMethod() !== 'POST') {
            
            if (is_null($request->request->get('password'))) {
                $entity->password = '';
            }
            return [
                'data' => $this->prepareData($entity),
                'errors' => []
            ];            
        }
        
        if (is_null($request->request->get('change_password'))) {
            $request->request->remove('password');
        }
        
        //TODO filter request to prepare data
        //Add EntityFilterableInterface to the controller to filter request using the types in the entity's field
        //Add FilterableInterface to the controller to filter request for the controller's 
        //Nullify empty strings if nullable (not required, and no default)
        //Make it so that you can define child entities, maybe use relations or allow array config with keys
        $data = array_merge($request->request->all(), $request->files->all());
        if (isset($data['date_publish'])) {
            $data['date_publish'] = new DateTime($data['date_publish']);
        }

        //Translate text NULL into real null value
        $relations = $entity->relations($this->repository, $entity);
        foreach ($relations as $name => $relation) {
            if ($relation instanceof BelongsTo) {
                if ($data[$relation->localKey()] === 'NULL') {
                    $data[$relation->localKey()] = null;
                }
            } 
        }

        $entity->data($data);

        if ($this->repository->validate($entity)) {
            
            $this->moveTempFiles($request->files->all(), $entity);
            $this->saveTempFiles($entity);
            $this->cleanFiles($entity);

            $this->createRelations($entity, $relations);
            
            //TODO Move image upload to here, not the repository
            if ($this->repository->update($entity, ['validate' => false, 'strict' => false]) === false) {
                throw new EntityNotSavedException;
            }
            
            
            return true;
        } else {
            if (is_null($request->request->get('password'))) {
                $entity->password = '';
            }
            
            $this->moveTempFiles($request->files->all(), $entity);
            
            return [
                'data' => $this->prepareData($entity),
                'errors' => $entity->errors()
            ];
        }
    }
}
