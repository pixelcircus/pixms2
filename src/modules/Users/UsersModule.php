<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Users;

use Pixms\CrudModules\Interfaces\ModuleInterface;
use Pixms\CrudModules\AbstractModule;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class UsersModule extends AbstractModule implements ModuleInterface
{
    protected $baseDir = __DIR__;
    protected $identifier = 'users';
    protected $mapper = 'Pixms\Modules\Users\UserMapper';
    protected $dataTableFactory = 'Pixms\Modules\Users\Tables\UserDataTableFactory';
    protected $crudService = 'Pixms\Modules\Users\UserCrudService';
    
    public function register() {
        parent::register();
    }

    public function getConfig()
    {
        $c = $this->getContainer();
        return [
            $this->getFormFactory() => [
                'definition' => function() use ($c) {
                    $request = $c->get('Symfony\Component\HttpFoundation\Request');
                    if ($request->attributes->get('id')) {
                        return new \Pixms\Modules\Users\Forms\UpdateFormFactory($c->get('Pixms\Url\UrlFactory'), $c->get('Symfony\Component\Translation\Translator'));
                    } else {
                        return new \Pixms\Modules\Users\Forms\CreateFormFactory($c->get('Pixms\Url\UrlFactory'), $c->get('Symfony\Component\Translation\Translator'));
                    }
                }
            ],
            $this->getEntity() => [
                'class' => 'Pixms\Modules\Users\Entities\UserEntity'
            ],
            $this->getMapper() => [
                'definition' => function() use ($c) {
                    return new \Pixms\Modules\Users\UserMapper($c->get('Spot\Locator'), 'Pixms\Modules\Users\Entities\UserEntity', $c->get('Pixms\AuthSquared\Interfaces\PasswordHandlerInterface'), $c->get('Symfony\Component\Translation\Translator'));
                },
                'singleton' => true
            ],
            $this->getDataTableFactory() => [
                'class' => 'Pixms\Modules\Users\Tables\UserDataTableFactory',
                'arguments' => ['Pixms\Url\UrlFactory', $this->getMapper(), 'Symfony\Component\Translation\Translator']
            ],
            $this->getCrudService() => [
                'class' => 'Pixms\Modules\Users\UserCrudService',
                'arguments' => [$this->getMapper(), 'CrudModulesDataTableDecorator', 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator']
            ],
        ];
    }
}
