<?php

return [
    'title_list' => 'Utilisateurs',
    'title_create' => 'Nouvel utilisateur',
    'title_update' => 'Mise à jour de l\'utilisateur',
    'title_change_password' => 'Changer le mot de passe',
    'note_change_password' => 'Le mot de passe ne peut être récupéré. Il peut seulement être réinitialiser à une nouvelle valeur.',
    'button_create' => 'Ajouter un utilisateur',
    'button_change_password' => 'Changer le mot de passe',
    'field_password_confirm' => 'Confirmation du mot de passe',
    'field_role' => 'Rôle',
    'callback_created' => 'L\'utilisateur a été créé.',
    'callback_updated' => 'L\'utilisateur a été mis à jour.',
    'callback_activated' => 'L\'utilisateur a été activé.',
    'callback_already_activated' => 'L\'utilisateur est déjà actif.',
    'callback_deactivated' => 'L\'utilisateur a été désactivé.',
    'callback_already_deactivated' => 'L\'utilisateur est déjà inactif.',
    'callback_deleted' => 'L\'utilisateur a été supprimé.',
    'callback_not_exists' => 'L\'utilisateur n\existe pas.',
    'choices_status_0' => 'Inactif',
    'choices_status_1' => 'Actif',
    'choices_status_2' => 'En attente',
    'choices_role_administrator' => 'Administrateur',
    'choices_role_editor' => 'Éditeur',
    'choices_role_observer' => 'Observateur',
    'error_password_length' => 'Le mot de passe doit être plus long que 6 caractères',
    'error_password_match' => 'Les champs de mot de passe ne corresponde pas',
];