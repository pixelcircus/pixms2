<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Users\Entities;

use Pixms\AuthSquared\Interfaces\UserInterface;
use Spot\Entity;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class UserEntity extends Entity implements UserInterface
{
    protected static $table = 'users';
    protected static $mapper = 'Pixms\Modules\Users\UserMapper';

    public static function fields()
    {
        return [
            'id' =>       ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'status' =>   ['type' => 'smallint', 'required' => true, 'default' => 0, 'choices' => [0, 1, 2]],
            'role' =>     ['type' => 'string', 'required' => true, 'default' => 'administrator', 'choices' => ['administrator', 'editor', 'observer']],
            'email' =>    ['type' => 'string', 'required' => true, 'unique' => true, 'validation' => ['email']],
            'image' =>    ['type' => 'file', 'validation' => ['image', 'minwidth' => 50, 'minheight' => 50]],
            'name' =>     ['type' => 'string', 'required' => true],
            'password' => ['type' => 'string', 'required' => true, 'length' => 60],
            'token' =>    ['type' => 'string', 'length' => 32],
            'token_expiration' => ['type' => 'datetime']
        ];
    }

    public function getPassword()
    {
        return $this->get('password');
    }

    public function getRole()
    {
        return $this->get('role');
    }
    
    public function getId()
    {
        return $this->get('id');
    }

}
