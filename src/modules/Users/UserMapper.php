<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Pixms\Modules\Users;

use Pixms\AuthSquared\Interfaces\PasswordHandlerInterface;
use Pixms\AuthSquared\Interfaces\UserInterface;
use Pixms\AuthSquared\Interfaces\UserRepositoryInterface;
use Pixms\Spot\PixmsMapper;
use Spot\EntityInterface;
use Spot\Locator;
use Symfony\Component\Translation\Translator;

/**
 * Description of PageMapper
 *
 * @author Sonia
 */
class UserMapper extends PixmsMapper implements UserRepositoryInterface
{
    protected $entityName = 'Pixms\Modules\Users\Entities\UserEntity';
    protected $pwd;
    protected $trans;
    
    public function __construct(Locator $locator, $entityName = null, PasswordHandlerInterface $pwd = null, Translator $trans = null)
    {
        $this->pwd = $pwd;
        $this->trans = $trans;
        parent::__construct($locator, $entityName);
    }
    
    public function getByUsername($username)
    {
        return $this->where(['email' => $username, 'status' => 1])->first();
    }
    
    public function getById($id)
    {
        return $this->get($id);
    }
    
    public function insert($entity, array $options = array())
    {
        $entity->password = $this->pwd->hash($entity->password);
        $entity->password_confirm = $entity->password;
        return parent::insert($entity, $options);
    }
    
    public function update(EntityInterface $entity, array $options = array())
    {
        if ($entity->isModified('password')) {
            $entity->password = $this->pwd->hash($entity->password);
            $entity->password_confirm = $entity->password;
        }
        return parent::update($entity, $options);
    }
    
    public function validate(EntityInterface $entity)
    {
        if ($entity->isModified('password') || $entity->isNew()) {
            if (mb_strlen($entity->password) < 6) {
                $entity->error('password', $this->trans->trans('error_password_length', [], 'users'));
            }
            if ($entity->password != $entity->password_confirm) {
                $entity->error('password', $this->trans->trans('error_password_match', [], 'users'));
            }
        }
        
        return parent::validate($entity);
    }

    public function getRights(UserInterface $user) {

        switch ($user->role) {
            case 'administrator':
            case 'editor':
                return ['pixms.access', 'pixms.view', 'pixms.create', 'pixms.update', 'pixms.delete'];
            case 'observer':
                return ['pixms.access', 'pixms.view'];
        }
        return [];
    }
}
