<?php

namespace Pixms\Modules\Users\Tables;

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Modules\Users\UserMapper;
use Pixms\Url\UrlFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataTableBuilder
 *
 * @author Sonia
 */
class UserDataTableFactory implements DataTableFactoryInterface
{

    protected $url;
    protected $repository;
    protected $trans;

    public function __construct(UrlFactory $url, UserMapper $repo, \Symfony\Component\Translation\Translator $trans)
    {
        $this->url = $url;
        $this->repository = $repo;
        $this->trans = $trans;
    }

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns([
            'name' => $this->trans->trans('field_name'), 
            'email' => $this->trans->trans('field_email'), 
            'role' => $this->trans->trans('field_role', [], 'users'), 
            'status' => $this->trans->trans('field_status')
        ])
        ->addAction(new DataTableAction('edit', $this->trans->trans('action_edit'), array($this, 'editUrl')))
        ->addAction(new DataTableAction('cancel-circle', $this->trans->trans('action_delete'), array($this, 'deleteUrl')));

        return $table;
    }
    
    public function editUrl($row)
    {
        return $this->url->create('/users/details/' . $row->id);
    }

    public function deleteUrl($row)
    {
        return $this->url->create('/users/delete/' . $row->id);
    }

}
