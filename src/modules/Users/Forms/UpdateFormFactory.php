<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Users\Forms;

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Url\UrlFactory;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class UpdateFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $trans;
    protected $entityName = "Pixms\Modules\Users\Entities\UserEntity";
    
    public function __construct(UrlFactory $url, \Symfony\Component\Translation\Translator $trans)
    {
        $this->url = $url;
        $this->trans = $trans;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        
        $edit_url = !empty($data['id']) ? $this->url->create('/uploads/details/users/{id}/image/') : null;
        $img_url = !empty($data['id']) ? $this->url->create('/i/fit/120/80/users/{id}/image/') : null;
        $back_uri = !empty($data['id']) ? $this->url->parseUri('/users/details/{id}') : null;
        $form->addSection('', ['two-columns'])
                 ->addItem('radio', $this->trans->trans('field_status'), 'status', ['choices' => $this->getChoices('status'), 'default' => 1])
                ->addItem('text', $this->trans->trans('field_name'), 'name')
                ->addItem('text', $this->trans->trans('field_email'), 'email')
                ->addItem('radio', $this->trans->trans('field_role', [], 'users'), 'role', ['choices' => $this->getChoices('role'), 'default' => 'administrator'])
                ->addItem('file', $this->trans->trans('field_image'), 'image', ['edit_url' => $edit_url, 'back_url' => $back_uri, 'preview_image' => $img_url])->end()
                ->addSection($this->trans->trans('title_change_password', [], 'users'), ['two-columns', 'subform'])
                ->addItem('note', $this->trans->trans('note_change_password', [], 'users'))
                ->addItem('password', $this->trans->trans('field_password'), 'password')
                ->addItem('password', $this->trans->trans('field_password_confirm', [], 'users'), 'password_confirm')
                ->addItem('submit', $this->trans->trans('button_change_password', [], 'users'), 'change_password')->end()
                ->addSection('', ['buttons'])
                ->addItem('link', $this->trans->trans('button_cancel'), null, ['url' => $this->url->create('/users'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', $this->trans->trans('button_submit'), null, [ 'input_attributes' => ['class' => 'ok']])->end();
        
        return $form;
    } 
    
    public function getChoices($field) {
        $fields = call_user_func(array($this->entityName, 'fields'));
        $choices = [];
        foreach ($fields[$field]['choices'] as $choice) {
            $choices[$choice] = $this->trans->trans('choices_'.$field.'_'.$choice, [], 'users');
        }
        return $choices;
    }
    
}
