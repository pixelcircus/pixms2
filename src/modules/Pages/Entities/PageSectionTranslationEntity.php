<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Entities;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class PageSectionTranslationEntity extends Entity
{

    protected static $table = 'pages_sections_translations';
    protected static $mapper = 'Pixms\Modules\Pages\Mappers\PageSectionTranslationMapper';

    //TODO: Have a getter/setter generator
    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'title' => ['type' => 'string', 'required' => true],
            'locale' => ['type' => 'string', 'unique' => 'page_section_id_locale', 'length' => 5, 'validation' => ['lengthBetween' => [2,5]]],
            //TODO Should be able to put required
            'page_section_id' => ['type' => 'integer', 'unique' => 'page_section_id_locale']
        ];
    }

    /**
     * Return defined fields of the entity
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'pageSection' => $mapper->belongsTo($entity, 'Pixms\Modules\Pages\Entities\PageSectionEntity', 'page_section_id')
        ];
    }

}
