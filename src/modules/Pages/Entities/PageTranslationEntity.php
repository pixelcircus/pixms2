<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Entities;

use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class PageTranslationEntity extends Entity
{

    protected static $table = 'pages_translations';
    protected static $mapper = 'Pixms\Modules\Pages\Mappers\PageTranslationMapper';

    //TODO: Have a getter/setter generator
    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'title' => ['type' => 'string', 'required' => true],
            //TODO Add new type WYSIWYG
            'description' => ['type' => 'text'],
            'locale' => ['type' => 'string', 'unique' => 'pages_id_locale', 'length' => 5, 'validation' => ['lengthBetween' => [2,5]]],
            //TODO Should be able to put required
            'pages_id' => ['type' => 'integer', 'unique' => 'pages_id_locale']
        ];
    }

    /**
     * Return defined fields of the entity
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'parent_page' => $mapper->belongsTo($entity, 'Pixms\Modules\Pages\Entities\PageEntity', 'pages_id')
        ];
    }

}
