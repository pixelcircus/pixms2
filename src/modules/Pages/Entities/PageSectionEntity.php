<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Entities;

use Pixms\Translation\TranslatableEntityInterface;
use Spot\Entity;
use Spot\EntityInterface;
use Spot\MapperInterface;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class PageSectionEntity extends Entity implements TranslatableEntityInterface
{

    protected static $table = 'pages_sections';
    protected static $mapper = 'Pixms\Modules\Pages\Mappers\PageSectionMapper';
    protected static $translationEntity = 'Pixms\Modules\Pages\Entities\PageSectionTranslationEntity';
    protected $translations = array();
    protected $locale = 'en';

    //TODO: Have a getter/setter generator
    public static function fields()
    {
        return [
            'id' => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'status' => ['type' => 'smallint', 'required' => true, 'default' => 0, 'choices' => array(0, 1, 2)],
            'pages_id' => ['type' => 'integer', 'index' => true]
        ];
    }

    /**
     * Return defined fields of the entity
     */
    public static function relations(MapperInterface $mapper, EntityInterface $entity)
    {
        return [
            'page' => $mapper->belongsTo($entity, 'Pixms\Modules\Pages\Entities\PageEntity', 'pages_id'),
            'translations' => $mapper->hasMany($entity, self::$translationEntity, 'page_section_id')
        ];
    }

    public static function getStatusChoices()
    {
        return [0 => 'Inactive', 1 => 'Active', 2 => 'Pending'];
    }
    
    public function setLocale($locale) {
        $this->locale = $locale;
        
        return $this;
    }
    
    public function getTitle() {
        $translation = $this->getTranslation();
        return is_null($translation) ? null : $translation->title;
    }
    
    //TODO Move to a trait
    public function getTranslations()
    {
        if (!count($this->translations)) {
            $translations = $this->get('translations') ?: [];
            foreach ($translations as $translation) {
                $this->translations[$translation->locale] = $translation;
            }
        }
        return $this->translations;
    }
    
    public function getTranslation($locale = null)
    {
        $translations = $this->getTranslations();
        return isset($translations[$locale ?: $this->locale]) ? $translations[$locale ?: $this->locale] : null;
    }
    
    public function setTranslations($data) {
        $translationEntity = self::$translationEntity;
        //Make sure relation is loaded
        //TODO: Should have a way to autoload some relations
        $this->getTranslations();
        foreach ($data as $locale => $translation) {
            if (isset($this->translations[$locale])) {
                $this->translations[$locale]->data($translation);
            } else {
                $translation['locale'] = $locale;
                //TODO Dehardcode
                //$translation['status'] = 0;
                $this->translations[$locale] = new $translationEntity($translation);
            }
        }
        //Important need to return translations
        //TODO maybe should return the relation or should better intergrate with relations
        return $this->translations;
    }

}
