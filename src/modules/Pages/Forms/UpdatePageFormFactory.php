<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Forms;

use Pixms\DataTables\DataTable;
use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Modules\Pages\Entities\PageEntity;
use Pixms\Modules\Pages\Mappers\PageMapper;
use Pixms\Url\UrlFactory;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class UpdatePageFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $dataTable;
    protected $repository;
    
    protected $entityName = "Pixms\Modules\Pages\Entities\PageEntity";

    public function __construct(UrlFactory $url, PageMapper $repository, DataTable $dataTable)
    {
        $this->url = $url;
        $this->repository = $repository;
        $this->dataTable = $dataTable;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $statusChoices = call_user_func(array($this->entityName, 'getStatusChoices'));

        $viewdata = ['datatable' => $this->dataTable];
        
        $form->addSection('', ['two-columns'])
                ->addItem('radio', 'Status', 'status', ['choices' => $statusChoices, 'default' => 1])
->addItem('select', 'Parent page', 'pages_id', ['objects' => $this->repository->parentList(), 'value_property' => 'id', 'label_property' => 'title', 'input_attributes' => ['placeholder' => 'No parent page'], 'none' => 'No parent page'])
                ->addItem('file', 'Image', 'image')->end();
        $this->translationForm($form, 'fr');
        $this->translationForm($form, 'en');
        $form->addSection('', ['full'])
                //TODO Dehardcode URL
                ->addItem('title', 'Section(s)')
                ->addItem('link', 'Add a section', null, ['url' => $this->url->create('/pages/details/{id}/section'), 'input_attributes' => ['class' => 'button']])
                ->addItem('view', '', null, ['view' => 'includes/datatable.twig', 'viewdata' => $viewdata])
                ->end()
                ->addSection('', ['buttons'])
                ->addItem('link', 'Cancel', null, ['url' => $this->url->create('/pages'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', 'Submit', null, [ 'input_attributes' => ['class' => 'ok']])->end();
        
        return $form;
    } 
    
    protected function translationForm($form, $locale) {
        $form->addSection($locale)
             ->addItem('text', 'Name', 'translations['.$locale.'][title]')
                ->end();
        
        return $form;
    }
    
}
