<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Forms;

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Modules\Pages\Entities\PageEntity;
use Pixms\Url\UrlFactory;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class PageSectionFormFactory implements FormFactoryInterface
{
    
    protected $url;
    
    protected $entityName = "Pixms\Modules\Pages\Entities\PageSectionEntity";
    protected $parent;

    public function __construct(UrlFactory $url, PageEntity $parent)
    {
        $this->url = $url;
        $this->parent = $parent;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $statusChoices = call_user_func(array($this->entityName, 'getStatusChoices'));
        //$pagesChoices = call_user_func(array($this->entityName, 'getStatusChoices'));
        $form->addSection('', ['two-columns'])
                ->addItem('radio', 'Status', 'status', ['choices' => $statusChoices, 'default' => 1])
               // ->addItem('select', 'Parent page', 'pages_id', ['choices' => ])
           //     ->addItem('file', 'Image', 'image')
                ->end();
        $this->translationForm($form, 'fr');
        $this->translationForm($form, 'en');
        $form->addSection('', ['buttons'])
                ->addItem('link', 'Cancel', null, ['url' => $this->url->create('/pages/details/'.$this->parent->primaryKey()), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', 'Submit', null, [ 'input_attributes' => ['class' => 'ok']])->end();
        
        return $form;
    } 
    
    protected function translationForm($form, $locale) {
        $form->addSection($locale)
             ->addItem('text', 'Name', 'translations['.$locale.'][title]')
                ->end();
        
        return $form;
    }
    
}
