<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages\Decorators;

use Pixms\Modules\Pages\Entities\PageEntity;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class PageTableDecorator extends PageEntity
{
    use \Pixms\CrudModules\Components\Reorder\OrderableDecoratorTrait;
    //TODO Have a trait for the status
    protected $twig;
    protected $url;
    
    public function setTwig($twig) {
        $this->twig = $twig;
        return $this;
    }
    
    public function setUrlFactory($url) {
        $this->url = $url;
        return $this;
    }
    
    public function getStatus() {
        return $this->twig->render('widgets/status.twig', array(
            'status' => $this->get('status'),
            'activate_url' => $this->url->create('/pages/activate/'.$this->get('id')),
            'deactivate_url' => $this->url->create('/pages/deactivate/'.$this->get('id'))
        ));
    }
    
    public function getPriority() {
        return $this->getOrderIcon();
    }
    
}
