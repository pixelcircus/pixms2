<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Pixms\Modules\Pages\Mappers;

use Pixms\Spot\PixmsMapper;
/**
 * Description of PageMapper
 *
 * @author Sonia
 */
class PageTranslationMapper extends PixmsMapper
{
    protected $entityName = 'Pixms\Modules\Pages\Entities\PageTranslationEntity';
    public function __construct(\Spot\Locator $locator, $entityName = null)
    {
        parent::__construct($locator, $entityName);
        $this->migrate();
    }
}
