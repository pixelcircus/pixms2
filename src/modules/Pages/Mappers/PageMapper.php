<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Pixms\Modules\Pages\Mappers;

use Pixms\Spot\PixmsMapper;
/**
 * Description of PageMapper
 *
 * @author Sonia
 */
class PageMapper extends PixmsMapper
{
    
    protected $entityName = 'Pixms\Modules\Pages\Entities\PageEntity';
    
    public function __construct(\Spot\Locator $locator, $entityName = null)
    {
        parent::__construct($locator, $entityName);
    }
    
    public function insert($entity, array $options = array())
    {
        $this->setOrder($entity);
        
        return parent::insert($entity, $options);
    }
    
    public function parentList() {
        //TODO Dehardcode locale
        $pages = $this->query('SELECT pages.* FROM pages INNER JOIN pages_translations ON pages_translations.pages_id = pages.id WHERE locale = "en" ORDER BY title');
        $pages = $this->with($pages, $this->entity(), ['translations']);
        return $pages;
    }
    
}
