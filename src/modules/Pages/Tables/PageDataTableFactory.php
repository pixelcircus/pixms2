<?php

namespace Pixms\Modules\Pages\Tables;

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Modules\Pages\Mappers\PageMapper;
use Laasti\Response\Engines\TemplateEngineInterface;
use Pixms\Url\UrlFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataTableBuilder
 *
 * @author Sonia
 */
class PageDataTableFactory implements DataTableFactoryInterface
{

    protected $url;
    protected $repository;
    
    protected $entityName = 'Pixms\Modules\Pages\Decorators\PageTableDecorator';

    public function __construct(UrlFactory $url, PageMapper $repo, TemplateEngineInterface $renderer)
    {
        $this->url = $url;
        $this->repository = $repo;
        
        $repo->setEntityName($this->entityName);
        $repo->eventEmitter()->on('afterLoad', function($entity, $repo) use ($renderer, $url) {
            $entity->setTwig($renderer);
            $entity->setUrlFactory($url);
        });
    }

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns(['priority' => 'Priorité','title' => 'Name', 'status' => 'Status'])
                ->addClasses('priority', ['icon-column'] )
                ->setAttributes(['data-reorder' => 'priority', 'data-reorder-url' => $this->url->create('/pages/reorder')])
                ->addAction(new DataTableAction('edit', 'Edit', array($this, 'editUrl')))
                ->addAction(new DataTableAction('cancel-circle', 'Delete', array($this, 'deleteUrl')));

        return $table;
    }
    
    public function getLinks() {
        return [
            [
                'label' => 'Create a page',
                'classes' => ['button', 'ok'],
                'url' => $this->url->create('/pages/details')
            ]
        ];
    }
    
    public function getResults() {
        return $this->repository->all()->order(['priority' => 'ASC']);
    }

    public function editUrl($row)
    {
        return $this->url->create('/pages/details/' . $row->id);
    }

    public function deleteUrl($row)
    {
        return $this->url->create('/pages/delete/' . $row->id);
    }

}
