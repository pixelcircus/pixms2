<?php

namespace Pixms\Modules\Pages\Tables;

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Modules\Pages\Entities\PageEntity;
use Pixms\Modules\Pages\Mappers\PageSectionMapper;
use Laasti\Response\Engines\TemplateEngineInterface;
use Pixms\Url\UrlFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataTableBuilder
 *
 * @author Sonia
 */
class PageSectionDataTableFactory implements DataTableFactoryInterface
{

    protected $url;
    
    protected $entityName = 'Pixms\Modules\Pages\Decorators\PageSectionTableDecorator';

    public function __construct(UrlFactory $url, PageSectionMapper $repo, TemplateEngineInterface $renderer)
    {
        $this->url = $url;
        
        $repo->setEntityName($this->entityName);
        $repo->eventEmitter()->on('afterLoad', function($entity, $repo) use ($renderer, $url) {
            $entity->setTwig($renderer);
            $entity->setUrlFactory($url);
        });
    }

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns(['title' => 'Name', 'status' => 'Status'])
                ->addAction(new DataTableAction('edit', 'Edit', array($this, 'editUrl')))
                ->addAction(new DataTableAction('cancel-circle', 'Delete', array($this, 'deleteUrl')));

        return $table;
    }

    public function editUrl($row)
    {
        return $this->url->create('/pages/details/{id}/section/' . $row->id);
    }

    public function deleteUrl($row)
    {
        return $this->url->create('/pages/delete/{id}/section/' . $row->id);
    }

}
