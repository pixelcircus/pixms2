<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Pages;

use League\Container\Container;
use League\Route\RouteCollection;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Laasti\Route\RouteCollector;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class PagesModule implements ModuleInterface
{
    protected $identifier = 'pages';
    protected $container;
    protected $router;

    public function __construct(Container $container, RouteCollector $router)
    {
        $this->container = $container;
        $this->router = $router;
    }
    
    public function boot() {
        $c = $this->container;
        $this->container->add('Pixms\Modules\Pages\Mappers\PageMapper')->withArgument('Spot\Locator');
        $this->container->add('Pixms\Modules\Pages\Forms\PageFormFactory')->withArguments(['Pixms\Url\UrlFactory', 'Pixms\Modules\Pages\Mappers\PageMapper']);
        $this->container->add('Pixms\Modules\Pages\Mappers\PageSectionMapper', null, true)->withArguments(['Spot\Locator']);
        //TODO better integration with the factory with automatic relations
        $this->container->add('PagesSectionsDatatable', function() use ($c) {
            $factory = $c->get('Pixms\Modules\Pages\Tables\PageSectionDataTableFactory');
            $request = $c->get('Symfony\Component\HttpFoundation\Request');
            $repo = $c->get('Pixms\Modules\Pages\Mappers\PageSectionMapper');
            
            return $factory->create($repo->where(['pages_id' => $request->attributes->get('id')]));
        });
        $this->container->add('Pixms\Modules\Pages\Forms\UpdatePageFormFactory')->withArguments(['Pixms\Url\UrlFactory', 'Pixms\Modules\Pages\Mappers\PageMapper', 'PagesSectionsDatatable']);
        /* $c = $this->container;
        $this->container->add('Pixms\Modules\Pages\Controllers\ReorderPageController', 'Pixms\Components\Reorder\Controllers\ReorderController')->withArgument('Pixms\Modules\Pages\Mappers\PageMapper');
        
        $this->container->add('PagesCreateView', 'Pixms\Forms\FormView')->withArguments(array('Pixms\Modules\Pages\Forms\PageFormFactory', 'New page'));
        $this->container->add('PagesCreateSuccessStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array('Pixms\Url\UrlFactory', '/pages', 'The page was successfully created.'));
        $this->container->add('PagesCreateStrategy', 'Pixms\Components\Crud\Strategies\RendererResponseStrategy')->withArguments(array('Laasti\Response\Engines\TemplateEngineInterface', 'PagesCreateView'));
        $this->container->add('PagesCreateController', 'Pixms\\Components\\Crud\\Controllers\\CreateController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesCreateStrategy', 'PagesCreateSuccessStrategy']);
        
        $this->container->add('PagesDataTableView', 'Pixms\DataTables\DataTableView')->withArguments(array('Pixms\Modules\Pages\Tables\PageDataTableFactory', 'Pages'));
        $this->container->add('PagesDataTableStrategy', 'Pixms\Components\Crud\Strategies\RendererResponseStrategy')->withArguments(array('Laasti\Response\Engines\TemplateEngineInterface', 'PagesDataTableView'));
        $this->container->add('PagesDataTableController', 'Pixms\\Views\\DisplayController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesDataTableStrategy']);
        
        $this->container->add('PagesUpdateView', 'Pixms\Forms\FormView')->withArguments(array('Pixms\Modules\Pages\Forms\UpdatePageFormFactory', 'Edit page'));
        $this->container->add('PagesUpdateSuccessStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array('Pixms\Url\UrlFactory', '/pages', 'The page was successfully updated.'));
        $this->container->add('PagesUpdateStrategy', 'Pixms\Components\Crud\Strategies\RendererResponseStrategy')->withArguments(array('Laasti\Response\Engines\TemplateEngineInterface', 'PagesUpdateView'));
        $this->container->add('PagesUpdateController', 'Pixms\\Components\\Crud\\Controllers\\UpdateController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesUpdateStrategy', 'PagesUpdateSuccessStrategy']);
                
        $this->container->add('PagesSectionsUpdateView', 'Pixms\Forms\FormView')->withArguments(array('Pixms\Modules\Pages\Forms\PageSectionFormFactory', 'Edit section'));
        $this->container->add('PagesSectionsUpdateSuccessStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array('Pixms\Url\UrlFactory', '/pages/details/{pages_id}', 'The section was successfully updated.'));
        $this->container->add('PagesSectionsUpdateStrategy', 'Pixms\Components\Crud\Strategies\RendererResponseStrategy')->withArguments(array('Laasti\Response\Engines\TemplateEngineInterface', 'PagesSectionsUpdateView'));
        $this->container->add('PagesSectionsUpdateController', 'Pixms\\Components\\Crud\\Controllers\\UpdateController')->withArguments(['Pixms\Modules\Pages\Mappers\PageSectionMapper', 'PagesSectionsUpdateStrategy', 'PagesSectionsUpdateSuccessStrategy']);
        
        $this->container->add('PagesSectionsCreateView', 'Pixms\Forms\FormView')->withArguments(array('Pixms\Modules\Pages\Forms\PageSectionFormFactory', 'New section'));
        $this->container->add('PagesSectionsCreateSuccessStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array('Pixms\Url\UrlFactory', '/pages/details/{pages_id}', 'The section was successfully created.'));
        $this->container->add('PagesSectionsCreateStrategy', 'Pixms\Components\Crud\Strategies\RendererResponseStrategy')->withArguments(array('Laasti\Response\Engines\TemplateEngineInterface', 'PagesSectionsCreateView'));
        $this->container->add('PagesSectionsCreateController', 'Pixms\\Components\\Crud\\Controllers\\CreateController')->withArguments(['Pixms\Modules\Pages\Mappers\PageSectionMapper', 'PagesSectionsCreateStrategy', 'PagesSectionsCreateSuccessStrategy']);
        
        $this->container->add('PagesDeleteStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array('Pixms\Url\UrlFactory', '/pages', 'The page was successfully deleted.'));
        $this->container->add('PagesDeleteController', 'Pixms\\Components\\Crud\\Controllers\\DeleteController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesDeleteStrategy']);
        $this->container->add('PagesSectionsDeleteStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array($this->container->get('Pixms\Url\UrlFactory'), '/pages/details/{pages_id}', 'The section was successfully deleted.'));
        $this->container->add('PagesSectionsDeleteController', 'Pixms\\Components\\Crud\\Controllers\\DeleteController')->withArguments(['Pixms\Modules\Pages\Mappers\PageSectionMapper', 'PagesSectionsDeleteStrategy']);
                
        $this->container->add('PagesStatusActivateStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array($this->container->get('Pixms\Url\UrlFactory'), '/pages', 'The page was successfully activated.'));
        $this->container->add('PagesStatusActivateController', 'Pixms\\Components\\Status\\Controllers\\StatusController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesStatusActivateStrategy']);
        $this->container->add('PagesStatusDeactivateStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array($this->container->get('Pixms\Url\UrlFactory'), '/pages', 'The page was successfully deactivated.'));
        $this->container->add('PagesStatusDeactivateController', 'Pixms\\Components\\Status\\Controllers\\StatusController')->withArguments(['Pixms\Modules\Pages\Mappers\PageMapper', 'PagesStatusDeactivateStrategy']);
        $this->container->add('PagesSectionsStatusActivateStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array($this->container->get('Pixms\Url\UrlFactory'), '/pages/details/{pages_id}', 'The section was successfully activated.'));
        $this->container->add('PagesSectionsStatusActivateController', 'Pixms\\Components\\Status\\Controllers\\StatusController')->withArguments(['Pixms\Modules\Pages\Mappers\PageSectionMapper', 'PagesSectionsStatusActivateStrategy']);
        $this->container->add('PagesSectionsStatusDeactivateStrategy', 'Pixms\Components\Crud\Strategies\RedirectSuccessStrategy')->withArguments(array($this->container->get('Pixms\Url\UrlFactory'), '/pages/details/{pages_id}', 'The section was successfully deactivated.'));
        $this->container->add('PagesSectionsStatusDeactivateController', 'Pixms\\Components\\Status\\Controllers\\StatusController')->withArguments(['Pixms\Modules\Pages\Mappers\PageSectionMapper', 'PagesSectionsStatusDeactivateStrategy']);
        */
        //Code on all pages
        //$this->registerRoutes($this->route);
    }
    
    public function getFormFactory($request) {
        if ($request->attributes->get('id', false)) {
            return 'Pixms\Modules\Pages\Forms\UpdatePageFormFactory';
        } else {
            return 'Pixms\Modules\Pages\Forms\PageFormFactory';
        }
    }
    
    public function getMapper() {
        return 'Pixms\Modules\Pages\Mappers\PageMapper';
    }
    
    public function getDataTableFactory() {
        return 'Pixms\Modules\Pages\Tables\PageDataTableFactory';
    }
    
    public function getCrudService() {
        return 'Pixms\CrudModules\CrudService';
    }
    
    public function getIdentifier() {
        return $this->identifier;
    }
    
    public function getNamespace() {
        $class = get_class($this);
        return substr($class, 0, strrpos($class, '\\'));
    }
}
