<?php

return [
    'title_list' => 'Fichiers',
    'title_create' => 'Nouveau fichier',
    'title_update' => 'Mise à jour du fichier',
    'button_create' => 'Ajouter un fichier',
    'callback_created' => 'Le fichier a été créé.',
    'callback_updated' => 'Le fichier a été mis à jour.',
    'callback_activated' => 'Le fichier a été activé.',
    'callback_already_activated' => 'Le fichier est déjà actif.',
    'callback_deactivated' => 'Le fichier a été désactivé.',
    'callback_already_deactivated' => 'Le fichier est déjà inactif.',
    'callback_deleted' => 'Le fichier a été supprimé.',
    'callback_not_exists' => 'Le fichier n\existe pas.',
    'choices_status_0' => 'Inactif',
    'choices_status_1' => 'Actif',
];