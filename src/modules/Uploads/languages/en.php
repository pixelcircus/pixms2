<?php

return [
    'title_list' => 'Uploads',
    'title_create' => 'New upload',
    'title_update' => 'Update upload',
    'button_create' => 'Add an upload',
    'callback_created' => 'The upload has been created.',
    'callback_updated' => 'The upload has been updated.',
    'callback_activated' => 'The upload has been activated.',
    'callback_already_activated' => 'The upload is already active.',
    'callback_deactivated' => 'The upload has been deactivated.',
    'callback_already_deactivated' => 'The upload is already inactive.',
    'callback_deleted' => 'The upload has been deleted.',
    'callback_not_exists' => 'The upload does not exist.',
    'choices_status_0' => 'Inactive',
    'choices_status_1' => 'Active',
];