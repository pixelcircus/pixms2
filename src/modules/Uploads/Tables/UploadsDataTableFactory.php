<?php

namespace Pixms\Modules\Uploads\Tables;

use Pixms\DataTables\DataTable;
use Pixms\DataTables\DataTableAction;
use Pixms\DataTables\DataTableFactoryInterface;
use Pixms\Url\UrlFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserDataTableBuilder
 *
 * @author Sonia
 */
class UploadsDataTableFactory implements DataTableFactoryInterface
{

    protected $url;

    public function __construct(UrlFactory $url)
    {
        $this->url = $url;
    }

    public function create($data)
    {
        $table = new DataTable($data);
        $table->addColumns(['title_fr' => 'Titre'])
                ->addAction(new DataTableAction('edit', 'Edit', array($this, 'editUrl')))
                ->addAction(new DataTableAction('cancel-circle', 'Delete', array($this, 'deleteUrl')));

        return $table;
    }

    public function editUrl($row)
    {
        return $this->url->create('/uploads/details/' . $row->primaryKey());
    }

    public function deleteUrl($row)
    {
        return $this->url->create('/uploads/delete/' . $row->primaryKey());
    }

}
