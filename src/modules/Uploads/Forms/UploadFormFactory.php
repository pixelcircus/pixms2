<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Uploads\Forms;

use Pixms\Forms\Form;
use Pixms\Forms\FormFactoryInterface;
use Pixms\Url\UrlFactory;
use Spot\MapperInterface;

/**
 * Description of CreateFormFactory
 *
 * @author Sonia
 */
class UploadFormFactory implements FormFactoryInterface
{
    
    protected $url;
    protected $service;
    protected $fileLocator;
    protected $formats = [];

    public function __construct(UrlFactory $url, \Pixms\CrudModules\CrudService $service, \Pixms\Files\EntityFileLocator $fileLocator , $formats = [])
    {
        $this->url = $url;
        $this->service = $service;
        $this->fileLocator = $fileLocator;
        $this->formats = $formats;
    }
    
    public function create($action, array $data, array $errors = []) {
        $form = $this->getMainForm($action, $data, $errors);
        $this->addButtons($form);
        return $form;
    }

    public function getMainForm($action, array $data, array $errors = [])
    {
        $form = new Form($action, 'post', $data, $errors, array(), array('enctype' => 'multipart/form-data'));
        $this->addLocaleForm($form, 'fr');
        $this->addLocaleForm($form, 'en');
        $this->addCropForm($form);
        
        return $form;
    }

    public function addButtons($form)
    {
        //Cancel should send back to back_url GET parameter if set
        $form->addSection('', ['buttons'])
                ->addItem('link', 'Cancel', null, ['url' => $this->url->create('/news'), 'input_attributes' => ['class' => 'button']])
                ->addItem('submit', 'Submit', null, [ 'input_attributes' => ['class' => 'ok']])->end();
        return $this;
    }
    
    public function addLocaleForm($form, $locale) {
        $form->addSection('', ['two-columns', 'line-before'])
                ->addItem('text', 'Title ('.$locale.')', 'title_'.$locale)->end();
    }
    public function addCropForm($form) {
        if (count($this->formats) === 0) {
            return;
        }
        
        $section = $form->addSection('', ['full', 'crop-image', 'line-before'])->addItem('title', 'Éditer l\'image');
        $section->addItem('select', 'Format', 'format', ['choices' => array_keys($this->formats)]);
        $sizes = getimagesizefromstring($this->fileLocator->getFileFromPath($this->service->getRequestEntity()->get('path')));
        $section->addItem('image', '', null, ['input_attributes' => [
            'src' => $this->fileLocator->getPublicUrlFromPath($this->service->getRequestEntity()->get('path'), 'maxwidth/1024'),
            'data-width' => $sizes[0],
            'data-height' => $sizes[1],
        ]]);
        foreach ($this->formats as $format_name => $size) {
            $form->addFormData('formats_size['.$format_name.']', implode('x', $size));
            $form->addFormData('formats_size['.$format_name.']', implode('x', $size));
            $section->addItem('hidden', '', 'formats_size['.$format_name.']');
            $section->addItem('hidden', '', 'formats['.$format_name.']');
        }
        $section->end();
    }
    
}
