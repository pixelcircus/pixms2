<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Uploads\Entities;

use Spot\Entity;

/**
 * Description of Pages
 *
 * @author Sonia
 */
class UploadEntity extends Entity
{

    protected static $table = 'uploads';
    protected static $mapper = 'Pixms\Spot\PixmsMapper';

    public static function fields()
    {
        return [
            'path' => ['type' => 'string', 'primary' => true, 'unique' => true],
            'title_fr' => ['type' => 'string'],
            'title_en' => ['type' => 'string'],
            'formats' => ['type' => 'json_array']
        ];
    }
}
