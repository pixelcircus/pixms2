<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Pixms\Modules\Uploads;

use League\Container\ContainerInterface;
use Pixms\CrudModules\AbstractModule;
use Pixms\CrudModules\Interfaces\ModuleInterface;
use Pixms\Spot\PixmsMapper;

/**
 * Description of UsersModule
 *
 * @author Sonia
 */
class UploadsModule extends AbstractModule implements ModuleInterface
{
    protected $baseDir = __DIR__;
    protected $identifier = 'uploads';
    protected $formFactory = 'Pixms\Modules\Uploads\Forms\UploadFormFactory';
    protected $entity = 'Pixms\Modules\Uploads\Entities\UploadEntity';
    protected $mapper = 'Pixms\Modules\Uploads\UploadsMapper';
    protected $dataTableFactory = 'Pixms\Modules\Uploads\Tables\UploadsDataTableFactory';

    public function getConfig()
    {
        $c = $this->getContainer();
        return [
            $this->getFormFactory() => [
                'class' => 'Pixms\Modules\Uploads\Forms\UploadFormFactory',
                'arguments' => ['Pixms\Url\UrlFactory', $this->getCrudService(), 'Pixms\Files\EntityFileLocator', $c->get('Uploads.config')['formats']]
            ],
            $this->getEntity() => [
                'class' => 'Pixms\Modules\Uploads\Entities\UploadEntity'
            ],
            $this->getMapper() => [
                'definition' => function() use ($c) {
                    return new PixmsMapper($c->get('Spot\Locator'), 'Pixms\Modules\Uploads\Entities\UploadEntity');
                },
                'singleton' => true
            ],
            $this->getDataTableFactory() => [
                'class' => 'Pixms\Modules\Uploads\Tables\UploadsDataTableFactory',
                'arguments' => ['Pixms\Url\UrlFactory']
            ],
            $this->getCrudService() => [
                'class' => 'Pixms\CrudModules\CrudService',
                'arguments' => [$this->getMapper(), 'CrudModulesDataTableDecorator', 'League\Flysystem\MountManager', 'Symfony\Component\HttpFoundation\Session\SessionInterface', 'Pixms\Files\EntityFileLocator'],
                'singleton' => true
            ],
        ];
    }

    public function getRoutes() {
        return [
            ['GET', '/{module:(?:'.$this->getIdentifier().')}/details/{path:path}', 'Pixms\CrudModules\Controllers\UpdateController::display'],
            ['POST', '/{module:(?:'.$this->getIdentifier().')}/details/{path:path}', 'Pixms\CrudModules\Controllers\UpdateController::submit'],
        ];
    }

}
