<?php

use Pixms\Core\Application;

define('BASEPATH', __DIR__.'/..');
// include the Composer autoloader
require BASEPATH.'/vendor/autoload.php';

Application::loadEnvironment(BASEPATH);

$config = require BASEPATH.'/autoload.php';

$app = new Application($config);
$request = $app->get('Symfony\Component\HttpFoundation\Request');
$app['locales.current'] = 'fr';//$request->getPreferredLanguage(['fr', 'en']);
Valitron\Validator::lang($app['locales.current']);
//Update auth messages to use lang files
$authConfig = $app->get('AuthSquared.config');
$translator = $app->get('Symfony\Component\Translation\Translator');
$translator->setLocale($app['locales.current']);
$translator->setFallbackLocales($app['locales.fallbacks']);
\Pixms\DataTables\DataTable::setGlobalConfig(new \Pixms\DataTables\DataTableConfig(['display_columns' => true, 'language' => $translator->getCatalogue()->all('datatables')]));
$authConfig['logged_out_message'] = $translator->trans('logged_out_message');
$authConfig['logged_in_message'] = $translator->trans('logged_in_message');
$authConfig['wrong_credentials_message'] = $translator->trans('wrong_credentials_message');
$authConfig['unauthorized_message'] = $translator->trans('unauthorized_message');
$app->get('Pixms\AuthSquared\AuthSquared')->getConfig()->set($authConfig);

$app->run();